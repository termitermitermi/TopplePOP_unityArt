using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class FrustumExpander : MonoBehaviour {
    public bool disable = false;
    public float hObliqueness = 0f;
    public float vObliqueness = 0f;
    public float hScale = 1f;
    public float vScale = 1f;
    private Camera _camera;

    void Awake() {
        _camera = GetComponent<Camera>();
        _camera.ResetProjectionMatrix();
    }

    public void OnPreCull() {
        _camera.ResetProjectionMatrix();
        if(disable) {
            return;
        }
        var origMatrix = _camera.projectionMatrix;
        var alteredMatrix = origMatrix;
        alteredMatrix[0,2] = hObliqueness;
        alteredMatrix[1,2] = vObliqueness;
        alteredMatrix[0,0] = origMatrix[0,0] * hScale;
        alteredMatrix[1,1] = origMatrix[1,1] * vScale;
        _camera.projectionMatrix = alteredMatrix;
    }

    void OnPreRender() {
        if(disable) {
            return;
        }
        _camera.ResetProjectionMatrix();
    }
}
