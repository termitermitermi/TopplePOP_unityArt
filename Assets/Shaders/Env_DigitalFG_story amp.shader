// Made with Amplify Shader Editor v1.9.4.4
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Env_DigitalFG_story"
{
	Properties
	{
		[HideInInspector]_GridUVScale("GridUVScale", Float) = 26
		_CurveAmount("CurveAmount", Float) = 1
		_curveOffset("curveOffset", Range( -50 , 50)) = 0
		[Header(Colors)]_GridColour1_base("GridColour1_base", Color) = (0.6470588,0.6039216,0.7529413,1)
		[HideInInspector]_unknownNegativeamount("unknownNegativeamount", Float) = -0.01
		_GridEdges("GridEdges", Color) = (0.2627958,0.249199,0.3018868,1)
		[Toggle]_curveToggle("curveToggle", Float) = 1
		_Grid_glowColor("Grid_glowColor", Color) = (0.2627958,0.249199,0.3018868,1)
		_gridLineEmission("gridLineEmission", Float) = 0
		_emissionMult("emissionMult", Float) = 0.25
		[Header(Pit Shading)]_PitHighlightHeight("PitHighlightHeight", Float) = 0.5
		_PitHighlightSize("PitHighlightSize", Float) = 0.5
		_PitShadowPosition("PitShadowPosition", Float) = 0.5
		_PitShadowHeight("PitShadowHeight", Float) = 0.5
		_GridEdgesPit("GridEdgesPit", Color) = (0,0,0,0)
		_GridColorPitDeep("GridColorPitDeep", Color) = (0,0,0,0)
		_GridColourPit_Shadow("GridColourPit_Shadow", Color) = (0.7294118,0.6980392,0.8117647,1)
		_GridColourPit_Hightlight("GridColourPit_Hightlight", Color) = (0.7294118,0.6980392,0.8117647,1)
		[HideInInspector]_GridUVOffset("GridUVOffset", Vector) = (0,0,0,0)
		[Header(Grid Settings)]_GridSize("GridSize", Range( 0 , 1)) = 0
		_GridSizeSmoothness("GridSizeSmoothness", Float) = 0
		_GridSizeSmoothnessFar("GridSizeSmoothnessFar", Float) = 0
		_GridRounding("GridRounding", Range( 0.01 , 1)) = 0
		[Header(Fog Settings)]_FogAmount("FogAmount", Range( 0 , 1)) = 0.5
		_FogHighNearColor("FogHighNearColor", Color) = (1,0,0.9933062,1)
		_FogHighFarColor("FogHighFarColor", Color) = (0,1,0.04610038,1)
		_FogLowNearColor("FogLowNearColor", Color) = (1,0,0,1)
		_FogLowFarColor("FogLowFarColor", Color) = (0,0.213161,1,1)
		_FogCutoutNear("FogCutoutNear", Float) = 0
		_FogCutoutFar("FogCutoutFar", Float) = 0.5
		_FogNear("FogNear", Float) = 0
		_FogFar("FogFar", Float) = 100
		_FogLow("FogLow", Float) = 0
		_FogHigh("FogHigh", Float) = 40
		_yOffset("yOffset", Float) = 500
		[HideInInspector]_uvroot("uv  root", Float) = 0
		_LightGlow_Fresnel("LightGlow_Fresnel", Color) = (1,0.7529413,0.2588235,1)
		_LightGlowShadowed_Fresnel("LightGlowShadowed_Fresnel", Color) = (1,0.7529413,0.2588235,1)
		_LightGlowSource("LightGlowSource", Vector) = (-0.35,2.36,52.4,0)
		_lineFillSmoothMetal("lineFillSmoothMetal", Vector) = (0,0,0,0)
		[Toggle]_Freeze("Freeze", Float) = 1
		_FreezeTime("FreezeTime", Float) = 0.5
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.5
		#define ASE_USING_SAMPLING_MACROS 1
		struct Input
		{
			float3 worldNormal;
			float2 uv_texcoord;
			float3 worldPos;
			float2 uv2_texcoord2;
			float4 screenPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float _VignetteIntensity;
		uniform float _VignetteLerpPower;
		uniform float _curveToggle;
		uniform float _CurveAmount;
		uniform float _unknownNegativeamount;
		uniform float _curveOffset;
		uniform float _uvroot;
		uniform float4 _GridColourPit_Hightlight;
		uniform float4 _GridColourPit_Shadow;
		uniform float4 _GridColorPitDeep;
		uniform float _PitShadowPosition;
		uniform float _PitShadowHeight;
		uniform float _PitHighlightHeight;
		uniform float _PitHighlightSize;
		uniform float4 _GridEdgesPit;
		uniform float4 _GridEdges;
		uniform float _GridSize;
		uniform float _GridSizeSmoothness;
		uniform float _GridSizeSmoothnessFar;
		uniform float _GridRounding;
		uniform float4 _GridColour1_base;
		uniform float4 _Grid_glowColor;
		uniform float _GridUVScale;
		uniform float2 _GridUVOffset;
		uniform float _Freeze;
		uniform float _FreezeTime;
		uniform float4 _LightGlowShadowed_Fresnel;
		uniform float4 _LightGlow_Fresnel;
		uniform float3 _LightGlowSource;
		uniform float4 _FogLowNearColor;
		uniform float4 _FogLowFarColor;
		uniform float _FogNear;
		uniform float _FogFar;
		uniform float4 _FogHighNearColor;
		uniform float4 _FogHighFarColor;
		uniform float _FogLow;
		uniform float _FogHigh;
		uniform float _yOffset;
		uniform float _FogCutoutNear;
		uniform float _FogCutoutFar;
		uniform float4 _VignetteColor;
		uniform float _VignetteSize;
		uniform float _FogAmount;
		uniform float _gridLineEmission;
		uniform float _emissionMult;
		uniform float4 _lineFillSmoothMetal;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 objToWorld1206 = mul( unity_ObjectToWorld, float4( ase_vertex3Pos, 1 ) ).xyz;
			float3 objToWorld1192 = mul( unity_ObjectToWorld, float4( ase_vertex3Pos, 1 ) ).xyz;
			float3 appendResult1205 = (float3(0.0 , ( ( ( _CurveAmount * _unknownNegativeamount ) * pow( ( ( objToWorld1192 - _WorldSpaceCameraPos ).z - _curveOffset ) , 2.0 ) ) + 0.0 ) , 0.0));
			float3 worldToObj1208 = mul( unity_WorldToObject, float4( ( objToWorld1206 + appendResult1205 ), 1 ) ).xyz;
			v.vertex.xyz = (( _curveToggle )?( worldToObj1208 ):( ase_vertex3Pos ));
			v.vertex.w = 1;
		}

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			SurfaceOutputStandard s1211 = (SurfaceOutputStandard ) 0;
			float4 GridColorEdge_Highlight1128 = _GridColourPit_Hightlight;
			float4 GridColorEdge_Shadow1129 = _GridColourPit_Shadow;
			float smoothstepResult1131 = smoothstep( ( _PitShadowPosition + -_PitShadowHeight ) , ( _PitShadowPosition + _PitShadowHeight ) , i.uv_texcoord.y);
			float4 lerpResult1137 = lerp( GridColorEdge_Shadow1129 , _GridColorPitDeep , smoothstepResult1131);
			float smoothstepResult1119 = smoothstep( ( _PitHighlightHeight + _PitHighlightSize ) , ( _PitHighlightHeight + -_PitHighlightSize ) , i.uv_texcoord.y);
			float4 lerpResult1125 = lerp( GridColorEdge_Highlight1128 , lerpResult1137 , smoothstepResult1119);
			float4 GridColour2414 = ( i.uv_texcoord.x > 1.0 ? _GridEdgesPit : _GridEdges );
			float3 ase_worldPos = i.worldPos;
			float lerpResult1088 = lerp( _GridSizeSmoothness , _GridSizeSmoothnessFar , (0.0 + (ase_worldPos.z - 0.0) * (1.0 - 0.0) / (500.0 - 0.0)));
			float2 uv_TexCoord3 = i.uv_texcoord + float2( -0.5,-0.5 );
			float2 break548 = frac( abs( uv_TexCoord3 ) );
			float a8_g13 = distance( break548.x , 0.5 );
			float b12_g13 = distance( break548.y , 0.5 );
			float k13_g13 = _GridRounding;
			float h14_g13 = ( max( ( k13_g13 - abs( ( a8_g13 - b12_g13 ) ) ) , 0.0 ) / k13_g13 );
			float smoothstepResult758 = smoothstep( _GridSize , ( _GridSize + lerpResult1088 ) , ( min( a8_g13 , b12_g13 ) - ( h14_g13 * h14_g13 * h14_g13 * k13_g13 * ( 1.0 / 6.0 ) ) ));
			float CheckerMaskFull405 = ( 1.0 - smoothstepResult758 );
			float4 lerpResult1071 = lerp( lerpResult1125 , GridColour2414 , CheckerMaskFull405);
			float4 GridColour1413 = _GridColour1_base;
			float4 lerpResult1047 = lerp( GridColour1413 , GridColour2414 , CheckerMaskFull405);
			float4 GridColour41072 = _Grid_glowColor;
			float2 temp_cast_0 = (_GridUVScale).xx;
			float2 uv2_TexCoord77 = i.uv2_texcoord2 * temp_cast_0 + _GridUVOffset;
			float2 GridUV82 = uv2_TexCoord77;
			float2 GridID134 = frac( ( floor( GridUV82 ) * float2( 1.001,1.001 ) ) );
			float mulTime164 = _Time.y * 1E-08;
			float2 temp_output_5_0_g12 = frac( ( ( GridID134 + (( _Freeze )?( ( _FreezeTime * 1E-08 ) ):( frac( mulTime164 ) )) ) * float2( 234.34,435.345 ) ) );
			float2 appendResult14_g12 = (float2(32.23 , 32.23));
			float dotResult6_g12 = dot( temp_output_5_0_g12 , ( temp_output_5_0_g12 + appendResult14_g12 ) );
			float2 break11_g12 = ( temp_output_5_0_g12 + dotResult6_g12 );
			float GridIDRandom158 = ( 1.0 - frac( ( break11_g12.x * break11_g12.y ) ) );
			float4 lerpResult1044 = lerp( ( ( GridColour41072 * GridIDRandom158 * 1.0 ) + GridColour1413 ) , GridColour2414 , CheckerMaskFull405);
			float4 temp_output_1027_0 =  ( i.uv_texcoord.x - 1.0 > _uvroot ? lerpResult1071 : i.uv_texcoord.x - 1.0 <= _uvroot && i.uv_texcoord.x + 1.0 >= _uvroot ? lerpResult1047 : lerpResult1044 ) ;
			float4 temp_output_1189_0 = ( i.uv_texcoord.y > 2.0 ? _LightGlowShadowed_Fresnel : _LightGlow_Fresnel );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV1154 = dot( ase_worldNormal, _LightGlowSource );
			float fresnelNode1154 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV1154, 5.0 ) );
			float4 lerpResult1184 = lerp( temp_output_1027_0 , float4( (temp_output_1189_0).rgb , 0.0 ) , ( ( 1.0 - CheckerMaskFull405 ) * saturate( fresnelNode1154 ) * ( i.uv_texcoord.y > 1.0 ? (temp_output_1189_0).a : 0.0 ) ));
			float4 FinalColor894 = ( lerpResult1184 + float4( 0,0,0,0 ) );
			float4 OUT_Albedo75 = FinalColor894;
			float depthFromOrigin975 = ase_worldPos.z;
			float smoothstepResult984 = smoothstep( _FogNear , _FogFar , depthFromOrigin975);
			float4 lerpResult991 = lerp( _FogLowNearColor , _FogLowFarColor , smoothstepResult984);
			float4 fogColorLow995 = lerpResult991;
			float smoothstepResult983 = smoothstep( _FogNear , _FogFar , depthFromOrigin975);
			float4 lerpResult988 = lerp( _FogHighNearColor , _FogHighFarColor , smoothstepResult983);
			float4 fogColorHigh993 = lerpResult988;
			float smoothstepResult994 = smoothstep( ( 0.0 + _FogLow ) , ( 0.0 + _FogHigh ) , ( ase_worldPos.y % _yOffset ));
			float4 lerpResult997 = lerp( fogColorLow995 , fogColorHigh993 , smoothstepResult994);
			float smoothstepResult1001 = smoothstep( _FogCutoutNear , _FogCutoutFar , depthFromOrigin975);
			float lerpResult1003 = lerp( 0.0 , (lerpResult997).a , smoothstepResult1001);
			float4 appendResult1004 = (float4((lerpResult997).rgb , lerpResult1003));
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 VignetteCoord7_g48 = ( ( (ase_screenPosNorm).xy / ase_screenPosNorm.w ) - float2( 0.5,0.5 ) );
			float dotResult9_g48 = dot( VignetteCoord7_g48 , VignetteCoord7_g48 );
			float Vignette_rf13_g48 = ( sqrt( dotResult9_g48 ) * _VignetteSize );
			float Vignette_rf_2_117_g48 = ( ( Vignette_rf13_g48 * Vignette_rf13_g48 ) + 1.0 );
			float Vignette_e22_g48 = ( 1.0 / pow( Vignette_rf_2_117_g48 , _VignetteIntensity ) );
			float temp_output_29_0_g48 = pow( ( _VignetteColor.a * ( 1.0 - Vignette_e22_g48 ) ) , _VignetteLerpPower );
			float vigfinal1014 = saturate( temp_output_29_0_g48 );
			float4 lerpResult1006 = lerp( appendResult1004 , float4( 0,0,0,0 ) , vigfinal1014);
			float4 fogColor1012 = lerpResult1006;
			float4 lerpResult1019 = lerp( OUT_Albedo75 , fogColor1012 , ( (fogColor1012).w * _FogAmount ));
			float4 FogOut1147 = lerpResult1019;
			s1211.Albedo = ( FogOut1147 + ( CheckerMaskFull405 * _gridLineEmission ) ).xyz;
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			s1211.Normal = ase_normWorldNormal;
			s1211.Emission = ( ( FogOut1147 + ( CheckerMaskFull405 * _gridLineEmission ) ) * _emissionMult ).xyz;
			float2 lerpResult1214 = lerp( (_lineFillSmoothMetal).xy , (_lineFillSmoothMetal).zw , CheckerMaskFull405);
			float2 break1219 = lerpResult1214;
			s1211.Metallic = break1219.y;
			s1211.Smoothness = break1219.x;
			s1211.Occlusion = 1.0;

			data.light = gi.light;

			UnityGI gi1211 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g1211 = UnityGlossyEnvironmentSetup( s1211.Smoothness, data.worldViewDir, s1211.Normal, float3(0,0,0));
			gi1211 = UnityGlobalIllumination( data, s1211.Occlusion, s1211.Normal, g1211 );
			#endif

			float3 surfResult1211 = LightingStandard ( s1211, viewDir, gi1211 ).rgb;
			surfResult1211 += s1211.Emission;

			#ifdef UNITY_PASS_FORWARDADD//1211
			surfResult1211 -= s1211.Emission;
			#endif//1211
			c.rgb = surfResult1211;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.5
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				float3 worldNormal : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19404
Node;AmplifyShaderEditor.CommentaryNode;247;3308.592,339.8871;Inherit;False;780.4713;424.22;Comment;5;184;77;80;334;1224;1. Create Grid UV;0.55,0.55,0.55,1;0;0
Node;AmplifyShaderEditor.Vector2Node;334;3358.788,595.1591;Inherit;False;Property;_GridUVOffset;GridUVOffset;21;1;[HideInInspector];Create;True;0;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;80;3330.592,425.987;Inherit;False;Property;_GridUVScale;GridUVScale;1;1;[HideInInspector];Create;True;0;0;0;False;0;False;26;50;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;77;3618.594,493.8871;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;197;3244.19,-92.95142;Inherit;False;1709.255;370.1288;Comment;11;158;194;166;134;382;154;164;135;136;685;962;Create Unique per-grid ids then has result against time;0.55,0.55,0.55,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;82;4053.656,554.2724;Inherit;True;GridUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;3274.465,-54.35449;Inherit;False;82;GridUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FloorOpNode;135;3274.83,24.25953;Inherit;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;11;1138.379,-868.7937;Inherit;False;1336.138;631.6389;Comment;16;561;562;563;559;557;564;548;544;3;758;759;760;1040;1088;1090;405;Grid Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;962;3449.921,-29.09545;Inherit;False;212;183;not a magic number, just need a very small amount of >1 to be unique;1;143;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;143;3499.921,20.90455;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;1.001,1.001;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;3;1158.379,-811.7937;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;164;3648,192;Inherit;False;1;0;FLOAT;1E-08;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1224;3840,400;Inherit;False;Property;_FreezeTime;FreezeTime;59;0;Create;True;0;0;0;False;0;False;0.5;67.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;154;3631.667,20.86939;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.AbsOpNode;544;1368.178,-815.4957;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FractNode;382;3853.617,203.7175;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1225;4048,432;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1E-08;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;134;3788.69,-18.74146;Inherit;True;GridID;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FractNode;1040;1153.495,-529.7375;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WorldPosInputsNode;1087;1385.681,-1109.244;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ToggleSwitchNode;1223;4208,336;Inherit;False;Property;_Freeze;Freeze;58;0;Create;True;0;0;0;False;0;False;1;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;166;4045.517,-17.57153;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.BreakToComponentsNode;548;1281.178,-533.4956;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;759;1452.689,-702.7058;Inherit;False;Property;_GridSizeSmoothness;GridSizeSmoothness;27;0;Create;True;0;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1090;1457.681,-627.2435;Inherit;False;Property;_GridSizeSmoothnessFar;GridSizeSmoothnessFar;28;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;1089;1614.681,-1075.244;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;500;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;194;4290.614,-17.73145;Inherit;True;randomhash21;-1;;12;878a710f7304f044c90dd520ea2b6a70;0;1;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;557;1420.105,-445.1812;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;564;1353.901,-349.2025;Inherit;False;Property;_GridRounding;GridRounding;29;0;Create;True;0;0;0;False;0;False;0;0.343;0.01;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;559;1417.105,-542.1814;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1088;1747.681,-674.2435;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1135;5983.126,-3284.782;Inherit;False;Property;_PitShadowHeight;PitShadowHeight;16;0;Create;True;0;0;0;False;0;False;0.5;0.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;562;1732.105,-758.1815;Inherit;False;Property;_GridSize;GridSize;26;1;[Header];Create;True;1;Grid Settings;0;0;False;0;False;0;0.05;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;685;4501.605,-3.011472;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;563;1658.901,-468.2026;Inherit;True;sdf_smoothmin;-1;;13;6fd99f77842ae3346a47e1454d6b83ee;0;3;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;460;1526.867,-2572.275;Inherit;False;1065.343;587.9131;;9;399;413;1126;1128;398;1153;414;858;1151;I/O Inputs/Outputs;1,1,1,0;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;760;1967.689,-673.7058;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;1136;6159.231,-3243.226;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1063;1427.872,-2385.539;Inherit;False;Property;_Grid_glowColor;Grid_glowColor;8;0;Create;True;0;0;0;True;0;False;0.2627958,0.249199,0.3018868,1;0.8254718,0.9349483,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;1121;5967.113,-3569.952;Inherit;False;Property;_PitHighlightSize;PitHighlightSize;14;0;Create;True;0;0;0;False;0;False;0.5;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1133;5988.301,-3364.502;Inherit;False;Property;_PitShadowPosition;PitShadowPosition;15;0;Create;True;0;0;0;False;0;False;0.5;0.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1127;2816.838,-2394.496;Inherit;False;Property;_GridColourPit_Shadow;GridColourPit_Shadow;19;0;Create;True;0;0;0;True;0;False;0.7294118,0.6980392,0.8117647,1;1,0.5803921,0.5803921,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;1117;5941.634,-3977.644;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NegateNode;1123;6143.218,-3528.395;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1132;6301.635,-3381.397;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1134;6298.45,-3284.783;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;1129;2833.838,-2204.496;Inherit;False;GridColorEdge_Shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;858;1649.269,-2414.54;Inherit;False;Property;_GridEdgesPit;GridEdgesPit;17;0;Create;True;0;0;0;True;0;False;0,0,0,0;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;398;1648.041,-2254.17;Inherit;False;Property;_GridEdges;GridEdges;6;0;Create;True;0;0;0;True;0;False;0.2627958,0.249199,0.3018868,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;1151;1655.411,-2539.254;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;399;2325.772,-2398.296;Inherit;False;Property;_GridColour1_base;GridColour1_base;4;1;[Header];Create;True;1;Colors;0;0;True;0;False;0.6470588,0.6039216,0.7529413,1;0.4025899,0.5231255,0.7830188,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;1072;1416.39,-2191.252;Inherit;False;GridColour4;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;158;4741.507,-8.902587;Inherit;True;GridIDRandom;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1120;5975.61,-3647.457;Inherit;False;Property;_PitHighlightHeight;PitHighlightHeight;13;1;[Header];Create;True;1;Pit Shading;0;0;False;0;False;0.5;0.98;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;758;2107.689,-751.7058;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1126;2554.838,-2400.496;Inherit;False;Property;_GridColourPit_Hightlight;GridColourPit_Hightlight;20;0;Create;True;0;0;0;True;0;False;0.7294118,0.6980392,0.8117647,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;1122;6285.622,-3666.567;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1124;6282.437,-3569.953;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;1131;6483.185,-3416.434;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;1138;6124.259,-3005.503;Inherit;False;1129;GridColorEdge_Shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1062;5864.585,-2563.807;Inherit;False;1072;GridColour4;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1029;5870,-2477.639;Inherit;True;158;GridIDRandom;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1143;5918.024,-2281.104;Inherit;False;Constant;_Float3;Float 3;49;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;413;2341.772,-2225.296;Inherit;False;GridColour1;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;1128;2556.838,-2208.496;Inherit;False;GridColorEdge_Highlight;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Compare;1153;1904.411,-2355.254;Inherit;False;2;4;0;FLOAT;0;False;1;FLOAT;1;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;561;2352.106,-753.1813;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1084;6118.939,-2875.783;Inherit;False;Property;_GridColorPitDeep;GridColorPitDeep;18;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.3098039,0.1326698,0.08235288,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexCoordVertexDataNode;1188;6722.87,-2163.561;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;1190;6861.175,-2054.359;Inherit;False;Property;_LightGlowShadowed_Fresnel;LightGlowShadowed_Fresnel;55;0;Create;True;0;0;0;False;0;False;1,0.7529413,0.2588235,1;0.2128124,0,0.681,0.3960784;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;1156;6912.629,-1876.35;Inherit;False;Property;_LightGlow_Fresnel;LightGlow_Fresnel;54;0;Create;True;0;0;0;False;0;False;1,0.7529413,0.2588235,1;0.5566037,0.4384567,0.5560988,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;973;3538.968,-4081.061;Inherit;False;2370.946;1416.587;Comment;41;1012;1006;1011;1007;1008;980;986;1002;1010;998;999;975;985;981;997;976;978;1003;977;982;984;988;1001;990;989;992;993;994;995;996;979;1004;983;1000;1005;974;1009;987;991;1148;1167;Build Fog Matrix;1,1,1,1;0;0
Node;AmplifyShaderEditor.SmoothstepOpNode;1119;6467.172,-3701.603;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1137;6539.485,-2945.711;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1130;6082.005,-3126.574;Inherit;False;1128;GridColorEdge_Highlight;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1060;6118.076,-2531.654;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0.125;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1146;6360.024,-2414.104;Inherit;False;413;GridColour1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;414;2084.041,-2353.171;Inherit;False;GridColour2;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector3Node;1158;7199.913,-2389.077;Inherit;False;Property;_LightGlowSource;LightGlowSource;56;0;Create;True;0;0;0;False;0;False;-0.35,2.36,52.4;0,0.27,1;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Compare;1189;7165.973,-2116.759;Inherit;False;2;4;0;FLOAT;0;False;1;FLOAT;2;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;405;2224,-416;Inherit;True;CheckerMaskFull;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;1051;6439.787,-2784.122;Inherit;False;414;GridColour2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1033;6109.631,-2659.084;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1125;6709.946,-2965.764;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1061;6628.041,-2435.436;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;1049;6313.314,-2653.786;Inherit;False;413;GridColour1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;974;3680.44,-4032.36;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;1160;7527.072,-2131.033;Inherit;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;1171;7428.399,-2487.744;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;1154;7454.712,-2400.333;Inherit;False;Standard;WorldNormal;LightDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;1170;7601.758,-1938.766;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;1071;6878.275,-2887.006;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;1047;6879.746,-2664.423;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;1044;6872.44,-2428.317;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;1021;7067.607,-2960.923;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;975;3826.442,-3878.054;Inherit;False;depthFromOrigin;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1037;7104.528,-2842.025;Inherit;False;Property;_uvroot;uv  root;50;1;[HideInInspector];Create;True;0;0;0;False;0;False;0;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;1186;7680.773,-2453.458;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;1187;7727.573,-2324.759;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Compare;1164;7947.279,-1958.074;Inherit;False;2;4;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCIf;1027;7423.979,-2857.319;Inherit;False;6;0;FLOAT;0;False;1;FLOAT;2;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;978;3646.082,-2820.109;Inherit;False;975;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;977;3642.123,-3261.149;Inherit;False;Property;_FogFar;FogFar;46;0;Create;True;0;0;0;False;0;False;100;45.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;976;3588.968,-3395.525;Inherit;False;975;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;979;3643.952,-3329.429;Inherit;False;Property;_FogNear;FogNear;45;0;Create;True;0;0;0;False;0;False;0;45.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;1159;7534.197,-2211.2;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1185;7891.373,-2417.058;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;981;4108.269,-3387.316;Inherit;False;Property;_FogLow;FogLow;47;0;Create;True;0;0;0;False;0;False;0;-7.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;986;3590.958,-3182.753;Inherit;False;Property;_FogHighNearColor;FogHighNearColor;39;0;Create;True;0;0;0;False;0;False;1,0,0.9933062,1;0.7960785,0.6666667,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;984;3881.128,-3453.817;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;985;4120.237,-3237.512;Inherit;False;Property;_FogHigh;FogHigh;48;0;Create;True;0;0;0;False;0;False;40;0.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;983;3848.776,-3021.323;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;987;3592.888,-3734.781;Inherit;False;Property;_FogLowNearColor;FogLowNearColor;41;0;Create;True;0;0;0;False;0;False;1,0,0,1;0.2109997,0.08951496,0.2067372,0.3176471;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;980;3592.821,-3562.334;Inherit;False;Property;_FogLowFarColor;FogLowFarColor;42;0;Create;True;0;0;0;False;0;False;0,0.213161,1,1;0.2028477,0.1108933,0.2641506,0.8509804;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;982;3590.566,-3016.667;Inherit;False;Property;_FogHighFarColor;FogHighFarColor;40;0;Create;True;0;0;0;False;0;False;0,1,0.04610038,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;1009;3844.972,-3762.889;Inherit;False;Property;_yOffset;yOffset;49;0;Create;True;0;0;0;False;0;False;500;-1.74;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1184;8018.773,-2747.258;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;992;4283.224,-3276.087;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;990;4285.224,-3410.087;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;988;4093.344,-3127.83;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;991;4084.34,-3576.452;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleRemainderNode;1148;4103.385,-3837.084;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1155;8277.439,-2842.686;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;893;6382.446,-1652.507;Inherit;False;1369.719;787.095;Add Vignette;4;895;1014;1013;75;Add Vignette;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;995;4244.34,-3581.452;Inherit;False;fogColorLow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;993;4253.092,-3133.719;Inherit;False;fogColorHigh;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;994;4470.488,-3400.654;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;894;8414.496,-2812.354;Inherit;False;FinalColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;998;4751.412,-3191.074;Inherit;False;Property;_FogCutoutFar;FogCutoutFar;44;0;Create;True;0;0;0;False;0;False;0.5;26.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;996;4719.384,-3351.49;Inherit;False;975;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;999;4743.412,-3274.074;Inherit;False;Property;_FogCutoutNear;FogCutoutNear;43;0;Create;True;0;0;0;False;0;False;0;-0.93;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;997;4704.717,-3477.302;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;895;6704.378,-1403.311;Inherit;False;894;FinalColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;1000;4896.384,-3473.49;Inherit;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;1001;4938.384,-3345.49;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;1013;6922.185,-1407.899;Inherit;False;TopplePOP_Vignette;-1;;48;e417e30fb7e59f943a39358cb05c9034;0;1;1;COLOR;0,0,0,0;False;2;COLOR;0;FLOAT;37
Node;AmplifyShaderEditor.RegisterLocalVarNode;1014;7458.801,-1272.239;Inherit;False;vigfinal;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1003;5099.384,-3489.49;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;1002;4891.384,-3614.49;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;1004;5231.384,-3618.49;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;1005;5232.383,-3184.5;Inherit;False;1014;vigfinal;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;1191;8224,-2080;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;1006;5414.526,-3527.129;Inherit;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TransformPositionNode;1192;8400,-2080;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;1193;8368,-1920;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;1012;5578.792,-3617.485;Inherit;True;fogColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;1194;8640,-2080;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;1020;7883.55,-1659.109;Inherit;False;1012;fogColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BreakToComponentsNode;1195;8816,-2080;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;1196;8656,-1792;Inherit;False;Property;_curveOffset;curveOffset;3;0;Create;True;0;0;0;False;0;False;0;0;-50;50;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;1016;8123.375,-1602.935;Inherit;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;75;7461.651,-1388.592;Inherit;False;OUT_Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;1017;8096,-1472;Inherit;False;Property;_FogAmount;FogAmount;38;1;[Header];Create;True;1;Fog Settings;0;0;False;0;False;0.5;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;1197;8976,-2032;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1198;8640,-2336;Inherit;False;Property;_CurveAmount;CurveAmount;2;0;Create;True;0;0;0;False;0;False;1;5.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1199;8576,-2256;Inherit;False;Property;_unknownNegativeamount;unknownNegativeamount;5;1;[HideInInspector];Create;True;0;0;0;False;0;False;-0.01;-0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1018;8377.079,-1585.635;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;350;7872,-1376;Inherit;True;75;OUT_Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;1200;9136,-2032;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1201;8848,-2336;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;1019;8632.299,-1684.379;Inherit;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;1226;9760,-2752;Inherit;True;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1202;9312,-2064;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;1147;8916.069,-1655.226;Inherit;False;FogOut;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;1230;9904,-2528;Inherit;False;Property;_gridLineEmission;gridLineEmission;9;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;1204;9392,-2256;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;1203;9472,-2064;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;1216;9408,-3312;Inherit;False;Property;_lineFillSmoothMetal;lineFillSmoothMetal;57;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.1,0.87,0.42,2;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;1086;9536,-2816;Inherit;True;1147;FogOut;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1227;10272,-2640;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;686;-111.5048,-2042.328;Inherit;False;6238.915;831;;5;797;679;703;449;862;Material Assembly;0.8844006,0.716,1,1;0;0
Node;AmplifyShaderEditor.TransformPositionNode;1206;9568,-2304;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;1205;9600,-2080;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;1218;9648,-3184;Inherit;False;False;False;True;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;1217;9648,-3280;Inherit;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;1212;9632,-2928;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1231;9920,-2912;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;449;-61.50473,-1989.725;Inherit;False;1464.02;518.2997;Set Arena Base Color/Uv Coordinates;5;444;443;447;436;456;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1207;9760,-2112;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;1214;10000,-3104;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RelayNode;1220;10064,-2928;Inherit;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;1222;10048,-2768;Inherit;False;Property;_emissionMult;emissionMult;10;0;Create;True;0;0;0;False;0;False;0.25;0.35;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;372;358.4586,-97.97237;Inherit;False;2763.161;571.6328;Comment;23;371;607;661;597;602;600;599;369;595;724;368;586;585;740;741;742;744;745;746;748;749;750;785;Mask Fade From Arena Edge;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;677;2551.582,-831.2716;Inherit;False;2121.561;612.4621;;23;953;952;365;348;669;355;277;675;752;671;676;673;356;672;674;583;333;956;138;959;960;961;957;Build Masked Noise;0.55,0.55,0.55,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;791;4720.2,-834.6758;Inherit;False;1634.738;698.295;Arena Edge Glow & Shadow Masks;18;831;830;833;834;836;835;822;826;823;838;839;840;841;843;842;844;845;790;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;703;1565.375,-1974.14;Inherit;False;1198.191;738.592;Add Edge Cells;8;687;756;689;688;695;696;690;757;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;456;197.0872,-1792.462;Inherit;False;370;280;Using a null texture to start building the material with the right texture coordinates;1;442;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;679;2877.998,-1942.523;Inherit;False;836.6689;501.7859;Add Masked Noise;5;620;665;668;678;616;Add Masked Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;797;3855.865,-1905.484;Inherit;False;838.1401;568.9064;Add Edge Glow;9;846;792;796;848;807;794;793;809;808;Add Edge Glow;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;862;4891.678,-1932.572;Inherit;False;945.23;594.9021;Add Edge Shadow;8;850;817;814;816;849;828;824;821;Add Edge Shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;891;299.269,-3401.862;Inherit;False;1116.691;1001.868;Vignette;19;887;890;888;889;872;883;886;885;884;878;881;879;880;877;876;866;865;864;912;Vignette;0.3962264,0,0.2693404,1;0;0
Node;AmplifyShaderEditor.TransformPositionNode;1208;9888,-2112;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.PosVertexDataNode;1209;9888,-2416;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;1219;10208,-3120;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1221;10368,-2880;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0.5;False;1;FLOAT4;0
Node;AmplifyShaderEditor.Vector2Node;585;443.291,211.4664;Inherit;False;Property;_ArenaEdgeFadeOffSet;ArenaEdgeFadeOffSet;31;0;Create;True;0;0;0;False;0;False;0,0;-1,-0.421;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;586;382.291,63.46643;Inherit;False;Property;_ArenaEdgeFadeTile;ArenaEdgeFadeTile;30;0;Create;True;0;0;0;False;0;False;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;368;656.6805,50.71037;Inherit;True;2;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;742;968.2983,25.01257;Inherit;False;Constant;_Range2;Range2;14;0;Create;True;0;0;0;False;0;False;1.5;1.39;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;745;1033.861,-0.5829544;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;744;986.29,104.1751;Inherit;False;Constant;_False;False;18;0;Create;True;0;0;0;False;0;False;1.5;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;741;951.5058,-51.75117;Inherit;False;Constant;_Range1;Range1;16;0;Create;True;0;0;0;False;0;False;-1;-1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareWithRange;740;1241.769,-28.96195;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;785;1481.936,254.0743;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;724;1737.964,-28.24151;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;369;1857.711,1.999554;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;595;1712.442,242.2935;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;602;2289.061,330.8647;Inherit;False;Property;_MaskedNoiseMaskRounding;MaskedNoiseMaskRounding;32;0;Create;True;0;0;0;False;0;False;1;0.0001;0.0001;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;138;2560.603,-794.2716;Inherit;True;158;GridIDRandom;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;600;2025.463,201.7137;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0.69;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;599;2018.463,-3.286336;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;148.31,-813.3118;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;597;2313.665,90.2977;Inherit;True;sdf_smoothmin;-1;;49;6fd99f77842ae3346a47e1454d6b83ee;0;3;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0.14;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;952;2586.965,-718.4575;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;911;382.4183,-778.4299;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;661;2610.217,183.5206;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;953;2747.965,-768.4575;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;959;2974.099,-800.2916;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.24;False;2;FLOAT;0.27;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;607;2825.608,181.2816;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;457;510.8754,-783.2979;Inherit;True;True;False;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;333;2823.582,-357.5205;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;960;3134.099,-788.2916;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;452;750.259,-785.4249;Inherit;False;Colour1_Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;957;3286.325,-763.3755;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;961;2967.099,-660.2916;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;674;3026.979,-365.6934;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;583;2992.625,-290.0443;Inherit;False;371;mask_ArenaEdgeFade;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;672;2911.556,-453.7439;Inherit;False;452;Colour1_Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;956;3173.965,-641.4575;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;676;3369.979,-346.6935;Inherit;False;Property;_MaskedNoise_Pulse;MaskedNoise_Pulse;34;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;356;3684.531,-573.5809;Inherit;False;Property;_MaskedNoiseAdd;MaskedNoiseAdd;33;0;Create;True;0;0;0;False;0;False;5;2.56;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;673;3220.592,-395.2104;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;752;3638.343,-419.2296;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;671;3422.241,-662.5963;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;675;3958.845,-442.005;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;277;3721.127,-653.4413;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;355;4003.584,-659.1146;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;838;5406.832,-380.3103;Inherit;False;Property;_EdgeGlowPosition;EdgeGlowPosition;12;0;Create;True;0;0;0;False;0;False;0.5;0.8;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;669;4197.438,-654.3865;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;839;5462.832,-273.3103;Inherit;False;Property;_EdgeGlowSize;EdgeGlowSize;22;0;Create;True;0;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;348;4333.583,-654.1912;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;841;5673.832,-397.3104;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;840;5672.832,-295.3103;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;831;4821.412,-515.9958;Inherit;False;Property;_EdgeShadowSize;EdgeShadowSize;25;0;Create;True;0;0;0;False;0;False;0.1;-0.32;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;843;5805.832,-291.3103;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;365;4473.666,-651.3895;Inherit;False;edgeeffector;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;844;5792.832,-393.3104;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;830;4765.412,-622.9958;Inherit;False;Property;_EdgeShadowPosition;EdgeShadowPosition;24;0;Create;True;0;0;0;False;0;False;0.5;0.053;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;842;5553.294,-519.3677;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;443;597.7728,-1703.272;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;834;5032.412,-639.9958;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;833;5031.412,-537.9958;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;688;2342.098,-1539.794;Inherit;True;452;Colour1_Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;845;5925.723,-494.911;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;620;2902.998,-1651.737;Inherit;True;365;edgeeffector;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;790;6119.899,-495.6906;Inherit;False;EdgeGlow;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;444;915.1555,-1800.596;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;756;2317.301,-1840.286;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;1,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;822;4911.875,-762.0533;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;689;2553.22,-1620.358;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;668;3129.896,-1647.575;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;808;4074.013,-1687.485;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;835;5151.412,-635.9958;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;836;5164.412,-533.9958;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;793;4089.334,-1615.332;Inherit;False;790;EdgeGlow;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;678;3282.804,-1652.017;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;665;2934.896,-1829.575;Inherit;False;413;GridColour1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;687;2579.341,-1929.479;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;1,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;809;4273.013,-1689.485;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;826;5284.302,-737.5966;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;846;4243.751,-1550.626;Inherit;True;452;Colour1_Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;823;5456.29,-787.6093;Inherit;False;EdgeShadow;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;616;3449.666,-1892.523;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;807;4407.013,-1651.485;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;821;5174.678,-1694.114;Inherit;False;823;EdgeShadow;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;794;3868.865,-1800.402;Inherit;False;Property;_EdgeGlow;EdgeGlow;11;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.6706856,0.6169412,0.8117647,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;848;4543.148,-1660.639;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;817;5155.057,-1600.013;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;796;4206.294,-1862.484;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;850;5388.464,-1690.507;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;792;4543.864,-1877.548;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;824;5365.875,-1567.67;Inherit;True;452;Colour1_Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;849;5568.029,-1688.148;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;828;5164.552,-1882.572;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;814;4950.678,-1828.114;Inherit;False;Property;_EdgeShadow;EdgeShadow;23;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.4220588,0.3756323,0.574,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;816;5654.908,-1876.075;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;184;3637.435,400.5681;Inherit;False;GridUVScale;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;748;1219.78,136.9597;Inherit;False;Property;_Range2min;Range2min;36;0;Create;True;0;0;0;False;0;False;1.5;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareWithRange;746;1513.25,-11.76253;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;885;815.0297,-2952.74;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;704;911.156,-2369;Inherit;False;Global;_ScreenShake;_ScreenShake;12;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;890;997.9152,-2688.155;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;1015;3833.339,-5164.694;Inherit;True;fogColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;912;822.7004,-3047.572;Inherit;False;Global;_VignetteSize;_VignetteSize;21;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;889;550.9229,-2768.425;Inherit;False;883;Vignette_rf_2_1;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;749;1217.381,207.7263;Inherit;False;Property;_Range2max;Range2max;35;0;Create;True;0;0;0;False;0;False;1.5;0.54;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;876;965.2097,-3345.98;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SqrtOpNode;878;720.2338,-3145.005;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;879;508.3879,-3143.774;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;881;1101.771,-3104.773;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;872;527.6422,-2611.654;Inherit;False;Global;_VignetteIntensity;_VignetteIntensity;22;0;Create;True;0;0;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;864;349.269,-3345.029;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;750;1226.976,274.8945;Inherit;False;Property;_True;True;37;0;Create;True;0;0;0;False;0;False;1.5;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;886;964.7752,-2951.285;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;865;529.941,-3335.862;Inherit;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;866;736.941,-3351.862;Inherit;True;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PowerNode;888;756.6379,-2706.671;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;877;1101.363,-3335.595;Inherit;False;VignetteCoord;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;883;1095.621,-2952.74;Inherit;False;Vignette_rf_2_1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;884;607.1304,-2942.563;Inherit;False;882;Vignette_rf;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;1031;3092.207,858.9398;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;1054;3376.975,850.7317;Inherit;False;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;1055;3578.975,798.7317;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1056;3726.975,823.7317;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;1057;3805.175,943.332;Inherit;True;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;11;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;1058;3572.076,1049.832;Inherit;False;Property;_Float0;Float 0;51;0;Create;True;0;0;0;False;0;False;-1;-22.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;371;2866.413,-7.215332;Inherit;True;mask_ArenaEdgeFade;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1065;1744.125,-289.9387;Inherit;True;Property;_TextureSample1;Texture Sample 1;52;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;882;1309.96,-3110.939;Inherit;False;Vignette_rf;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;880;316.5425,-3136.312;Inherit;False;877;VignetteCoord;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;887;1230.594,-2672.792;Inherit;False;Vignette_e;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;1098;7033.248,-3049.38;Inherit;False;-1;;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;1099;7701.11,-3050.438;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;1100;7332.892,-3020.61;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleAddOpNode;1113;7554.533,-3307.533;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;1116;7722.14,-3328.606;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1115;7322.849,-3232.289;Inherit;False;Property;_world_offset_glow;world_offset_glow;53;0;Create;True;0;0;0;False;0;False;0;0.12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;1112;7096.055,-3432.438;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleRemainderNode;1114;7331.497,-3345.647;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;400;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1145;6012.024,-2178.104;Inherit;False;Constant;_Float4;Float 4;49;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;989;4303.788,-3775.745;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;1007;4643.922,-3939.156;Inherit;False;fogYOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1011;4182.04,-3991.371;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;500;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;1010;4488.109,-3950.562;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;1008;4318.04,-4018.371;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1000;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;436;-45.49483,-1755.763;Inherit;True;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;442;247.0875,-1742.462;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;1;[HideInInspector];Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;447;594.2847,-1869.211;Inherit;False;414;GridColour2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;690;1968.615,-1493.018;Inherit;False;405;CheckerMaskFull;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;696;2081.586,-1574.46;Inherit;False;413;GridColour1;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;695;2168.82,-1482.823;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;757;1780.49,-1686.827;Inherit;False;414;GridColour2;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;1167;4028.879,-3671.712;Inherit;False;yOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;1210;10096,-2240;Inherit;False;Property;_curveToggle;curveToggle;7;0;Create;True;0;0;0;False;0;False;1;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomStandardSurface;1211;10448,-3264;Inherit;False;Metallic;Tangent;6;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,1;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;10800,-2800;Float;False;True;-1;3;ASEMaterialInspector;0;0;CustomLighting;Env_DigitalFG_story;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;;0;False;;False;0;False;;0;False;;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;12;all;True;True;True;True;0;False;;False;0;False;;255;False;;255;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;True;0;0;False;;0;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Absolute;0;;-1;-1;-1;-1;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;True;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;16;FLOAT4;0,0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;458;314.122,-957.6572;Inherit;False;803.0739;100;By multiplying then floor, then remainder (mod2), you can create a checker at each loop of the coordinates;0;;1,1,1,1;0;0
WireConnection;77;0;80;0
WireConnection;77;1;334;0
WireConnection;82;0;77;0
WireConnection;135;0;136;0
WireConnection;143;0;135;0
WireConnection;154;0;143;0
WireConnection;544;0;3;0
WireConnection;382;0;164;0
WireConnection;1225;0;1224;0
WireConnection;134;0;154;0
WireConnection;1040;0;544;0
WireConnection;1223;0;382;0
WireConnection;1223;1;1225;0
WireConnection;166;0;134;0
WireConnection;166;1;1223;0
WireConnection;548;0;1040;0
WireConnection;1089;0;1087;3
WireConnection;194;1;166;0
WireConnection;557;0;548;1
WireConnection;559;0;548;0
WireConnection;1088;0;759;0
WireConnection;1088;1;1090;0
WireConnection;1088;2;1089;0
WireConnection;685;0;194;0
WireConnection;563;4;559;0
WireConnection;563;5;557;0
WireConnection;563;6;564;0
WireConnection;760;0;562;0
WireConnection;760;1;1088;0
WireConnection;1136;0;1135;0
WireConnection;1123;0;1121;0
WireConnection;1132;0;1133;0
WireConnection;1132;1;1135;0
WireConnection;1134;0;1133;0
WireConnection;1134;1;1136;0
WireConnection;1129;0;1127;0
WireConnection;1072;0;1063;0
WireConnection;158;0;685;0
WireConnection;758;0;563;0
WireConnection;758;1;562;0
WireConnection;758;2;760;0
WireConnection;1122;0;1120;0
WireConnection;1122;1;1121;0
WireConnection;1124;0;1120;0
WireConnection;1124;1;1123;0
WireConnection;1131;0;1117;2
WireConnection;1131;1;1134;0
WireConnection;1131;2;1132;0
WireConnection;413;0;399;0
WireConnection;1128;0;1126;0
WireConnection;1153;0;1151;1
WireConnection;1153;2;858;0
WireConnection;1153;3;398;0
WireConnection;561;0;758;0
WireConnection;1119;0;1117;2
WireConnection;1119;1;1122;0
WireConnection;1119;2;1124;0
WireConnection;1137;0;1138;0
WireConnection;1137;1;1084;0
WireConnection;1137;2;1131;0
WireConnection;1060;0;1062;0
WireConnection;1060;1;1029;0
WireConnection;1060;2;1143;0
WireConnection;414;0;1153;0
WireConnection;1189;0;1188;2
WireConnection;1189;2;1190;0
WireConnection;1189;3;1156;0
WireConnection;405;0;561;0
WireConnection;1125;0;1130;0
WireConnection;1125;1;1137;0
WireConnection;1125;2;1119;0
WireConnection;1061;0;1060;0
WireConnection;1061;1;1146;0
WireConnection;1160;0;1189;0
WireConnection;1154;4;1158;0
WireConnection;1071;0;1125;0
WireConnection;1071;1;1051;0
WireConnection;1071;2;1033;0
WireConnection;1047;0;1049;0
WireConnection;1047;1;1051;0
WireConnection;1047;2;1033;0
WireConnection;1044;0;1061;0
WireConnection;1044;1;1051;0
WireConnection;1044;2;1033;0
WireConnection;975;0;974;3
WireConnection;1186;0;1171;0
WireConnection;1187;0;1154;0
WireConnection;1164;0;1170;2
WireConnection;1164;2;1160;0
WireConnection;1027;0;1021;1
WireConnection;1027;1;1037;0
WireConnection;1027;2;1071;0
WireConnection;1027;3;1047;0
WireConnection;1027;4;1044;0
WireConnection;1159;0;1189;0
WireConnection;1185;0;1186;0
WireConnection;1185;1;1187;0
WireConnection;1185;2;1164;0
WireConnection;984;0;976;0
WireConnection;984;1;979;0
WireConnection;984;2;977;0
WireConnection;983;0;978;0
WireConnection;983;1;979;0
WireConnection;983;2;977;0
WireConnection;1184;0;1027;0
WireConnection;1184;1;1159;0
WireConnection;1184;2;1185;0
WireConnection;992;1;985;0
WireConnection;990;1;981;0
WireConnection;988;0;986;0
WireConnection;988;1;982;0
WireConnection;988;2;983;0
WireConnection;991;0;987;0
WireConnection;991;1;980;0
WireConnection;991;2;984;0
WireConnection;1148;0;974;2
WireConnection;1148;1;1009;0
WireConnection;1155;0;1184;0
WireConnection;995;0;991;0
WireConnection;993;0;988;0
WireConnection;994;0;1148;0
WireConnection;994;1;990;0
WireConnection;994;2;992;0
WireConnection;894;0;1155;0
WireConnection;997;0;995;0
WireConnection;997;1;993;0
WireConnection;997;2;994;0
WireConnection;1000;0;997;0
WireConnection;1001;0;996;0
WireConnection;1001;1;999;0
WireConnection;1001;2;998;0
WireConnection;1013;1;895;0
WireConnection;1014;0;1013;37
WireConnection;1003;1;1000;0
WireConnection;1003;2;1001;0
WireConnection;1002;0;997;0
WireConnection;1004;0;1002;0
WireConnection;1004;3;1003;0
WireConnection;1006;0;1004;0
WireConnection;1006;2;1005;0
WireConnection;1192;0;1191;0
WireConnection;1012;0;1006;0
WireConnection;1194;0;1192;0
WireConnection;1194;1;1193;0
WireConnection;1195;0;1194;0
WireConnection;1016;0;1020;0
WireConnection;75;0;895;0
WireConnection;1197;0;1195;2
WireConnection;1197;1;1196;0
WireConnection;1018;0;1016;0
WireConnection;1018;1;1017;0
WireConnection;1200;0;1197;0
WireConnection;1201;0;1198;0
WireConnection;1201;1;1199;0
WireConnection;1019;0;350;0
WireConnection;1019;1;1020;0
WireConnection;1019;2;1018;0
WireConnection;1202;0;1201;0
WireConnection;1202;1;1200;0
WireConnection;1147;0;1019;0
WireConnection;1203;0;1202;0
WireConnection;1227;0;1226;0
WireConnection;1227;1;1230;0
WireConnection;1206;0;1204;0
WireConnection;1205;1;1203;0
WireConnection;1218;0;1216;0
WireConnection;1217;0;1216;0
WireConnection;1231;0;1086;0
WireConnection;1231;1;1227;0
WireConnection;1207;0;1206;0
WireConnection;1207;1;1205;0
WireConnection;1214;0;1217;0
WireConnection;1214;1;1218;0
WireConnection;1214;2;1212;0
WireConnection;1220;0;1231;0
WireConnection;1208;0;1207;0
WireConnection;1219;0;1214;0
WireConnection;1221;0;1220;0
WireConnection;1221;1;1222;0
WireConnection;368;0;586;0
WireConnection;368;1;585;0
WireConnection;745;0;368;1
WireConnection;740;0;745;0
WireConnection;740;1;741;0
WireConnection;740;2;742;0
WireConnection;740;3;368;1
WireConnection;740;4;744;0
WireConnection;785;0;368;2
WireConnection;724;0;740;0
WireConnection;369;0;724;0
WireConnection;595;0;785;0
WireConnection;600;0;595;0
WireConnection;599;0;369;0
WireConnection;597;4;599;0
WireConnection;597;5;600;0
WireConnection;597;6;602;0
WireConnection;952;0;138;0
WireConnection;911;0;6;1
WireConnection;661;0;597;0
WireConnection;953;0;138;0
WireConnection;953;1;952;0
WireConnection;959;0;953;0
WireConnection;607;0;661;0
WireConnection;457;0;911;0
WireConnection;960;0;959;0
WireConnection;452;0;457;0
WireConnection;957;0;960;0
WireConnection;961;0;953;0
WireConnection;674;0;333;0
WireConnection;956;0;957;0
WireConnection;956;1;961;0
WireConnection;673;0;672;0
WireConnection;673;1;674;0
WireConnection;673;2;583;0
WireConnection;752;0;676;0
WireConnection;752;1;356;0
WireConnection;671;1;956;0
WireConnection;671;2;673;0
WireConnection;675;0;356;0
WireConnection;675;1;752;0
WireConnection;277;0;671;0
WireConnection;355;0;277;0
WireConnection;355;1;675;0
WireConnection;669;0;355;0
WireConnection;348;0;669;0
WireConnection;841;0;838;0
WireConnection;841;1;839;0
WireConnection;840;0;838;0
WireConnection;840;1;839;0
WireConnection;843;0;840;0
WireConnection;365;0;348;0
WireConnection;844;0;841;0
WireConnection;443;0;442;0
WireConnection;834;0;830;0
WireConnection;834;1;831;0
WireConnection;833;0;830;0
WireConnection;833;1;831;0
WireConnection;845;0;842;1
WireConnection;845;1;844;0
WireConnection;845;2;843;0
WireConnection;790;0;845;0
WireConnection;444;0;447;0
WireConnection;444;1;443;0
WireConnection;756;0;757;0
WireConnection;756;1;696;0
WireConnection;756;2;695;0
WireConnection;689;0;688;0
WireConnection;668;0;620;0
WireConnection;835;0;834;0
WireConnection;836;0;833;0
WireConnection;678;0;668;0
WireConnection;687;0;444;0
WireConnection;687;1;756;0
WireConnection;687;2;689;0
WireConnection;809;0;808;0
WireConnection;826;0;822;1
WireConnection;826;1;835;0
WireConnection;826;2;836;0
WireConnection;823;0;826;0
WireConnection;616;0;687;0
WireConnection;616;1;665;0
WireConnection;616;2;678;0
WireConnection;807;0;809;0
WireConnection;807;1;793;0
WireConnection;848;0;807;0
WireConnection;848;2;846;0
WireConnection;796;0;616;0
WireConnection;850;0;821;0
WireConnection;850;2;817;0
WireConnection;792;0;796;0
WireConnection;792;1;794;0
WireConnection;792;2;848;0
WireConnection;849;0;850;0
WireConnection;849;2;824;0
WireConnection;828;0;792;0
WireConnection;816;0;828;0
WireConnection;816;1;814;0
WireConnection;816;2;849;0
WireConnection;184;0;80;0
WireConnection;746;0;740;0
WireConnection;746;1;748;0
WireConnection;746;2;749;0
WireConnection;746;3;750;0
WireConnection;746;4;740;0
WireConnection;885;0;884;0
WireConnection;885;1;884;0
WireConnection;890;1;888;0
WireConnection;876;0;866;0
WireConnection;878;0;879;0
WireConnection;879;0;880;0
WireConnection;879;1;880;0
WireConnection;881;0;878;0
WireConnection;881;1;912;0
WireConnection;886;0;885;0
WireConnection;865;0;864;0
WireConnection;866;0;865;0
WireConnection;866;1;864;4
WireConnection;888;0;889;0
WireConnection;888;1;872;0
WireConnection;877;0;876;0
WireConnection;883;0;886;0
WireConnection;1054;0;1031;0
WireConnection;1055;0;80;0
WireConnection;1055;1;1054;0
WireConnection;1056;0;1055;0
WireConnection;1056;1;334;0
WireConnection;1057;0;1056;0
WireConnection;1057;2;1058;0
WireConnection;371;0;607;0
WireConnection;882;0;881;0
WireConnection;887;0;890;0
WireConnection;1099;0;1027;0
WireConnection;1099;1;1098;0
WireConnection;1099;2;1100;3
WireConnection;1100;0;1098;0
WireConnection;1113;0;1114;0
WireConnection;1113;1;1115;0
WireConnection;1116;0;1113;0
WireConnection;1114;0;1112;2
WireConnection;1007;0;1010;0
WireConnection;1011;0;974;2
WireConnection;1010;0;1008;0
WireConnection;1010;1;1009;0
WireConnection;1008;0;1011;0
WireConnection;442;1;436;0
WireConnection;695;0;690;0
WireConnection;1167;0;1009;0
WireConnection;1210;0;1209;0
WireConnection;1210;1;1208;0
WireConnection;1211;0;1220;0
WireConnection;1211;2;1221;0
WireConnection;1211;3;1219;1
WireConnection;1211;4;1219;0
WireConnection;0;13;1211;0
WireConnection;0;11;1210;0
ASEEND*/
//CHKSM=9208990C4AAD6D9D2DE15B960DAAE501EBF7E979