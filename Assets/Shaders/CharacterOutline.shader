// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader"Custom/CharacterOutline" {
Properties {
    _CharacterOutlineColor ("CharacterOutlineColor", Color) = (0,0,0,1)
    _Outline("Outline width", Range(-0.2, 2.0)) = .005
    _OutlineZCheat ("ZCheat", Float) = 0.0
    _ZScale ("ZScale", float) = 0.25
    _ZOffset ("ZOffset", float) = 0.0
    _ZOffsetClip ("ZOffsetClip", float) = 0.0

}

SubShader {
    Tags { "RenderType"="Opaque" "Queue"="Geometry+1" }
    LOD 100

    Stencil {
        Ref 128
        Comp NotEqual
        Pass keep
        ReadMask 128
        WriteMask 128
    }

    Pass {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                UNITY_FOG_COORDS(0)
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _CharacterOutlineColor;
            float _OutlineZCheat;
            float _Outline;
            float _ZScale;
            float _ZOffset;
            float _ZOffsetClip;

            // should be set globally
            int _ExtraPerspectiveNSplits;
            float4 _ExtraPerspectiveSplitData;
            uniform float4 _ScreenShake[4];

            v2f vert (appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                float nearestSplitDist = 999.0;
                int worldID = -1;

                float4 world = mul( unity_ObjectToWorld, v.vertex );

                // scale by _ZScale unless in StoryDialog scene
                if(world.y > -300.0) {
                    world.z *= _ZScale;
                    world.z += _ZOffset;
                }

                // apply screenshake
                for(int i = 0; i < _ExtraPerspectiveNSplits; i++) {
                    float d = abs(world.x - _ExtraPerspectiveSplitData[i]);
                    if(d < nearestSplitDist) {
                        worldID = i;
                        nearestSplitDist = d;
                    }
                }

                if(worldID != -1) {
                    world.xyz += _ScreenShake[worldID].xyz;
                }

                // convert back to object space
                float4 objectPos = mul(unity_WorldToObject, world);

                // extrude outlines
                objectPos.xyz += normalize(v.normal) * _Outline;

                o.vertex = UnityObjectToClipPos(objectPos);

                if(world.y > -300.0) {
                    o.vertex.z += _ZOffsetClip;
                }

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                fixed4 col = _CharacterOutlineColor;
                UNITY_APPLY_FOG(i.fogCoord, col);
                UNITY_OPAQUE_ALPHA(col.a);
                return col;
            }
        ENDCG
    }
}

}
