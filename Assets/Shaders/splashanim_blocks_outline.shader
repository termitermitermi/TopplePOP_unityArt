// Made with Amplify Shader Editor v1.9.1.8
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "splashanim_blocks_outline"
{
	Properties
	{
		[Header(Colors)]_Color0("Color 0", Color) = (0.7450981,0.4313726,0.509804,1)
		_Color1("Color 1", Color) = (0.02352941,0.345098,0.5960785,1)
		_Color2("Color 2", Color) = (0.7529413,0.6470588,0.2666667,1)
		_Color3("Color 3", Color) = (0.7058824,0.3019608,0.1568628,1)
		_Color4("Color 4", Color) = (0.2196079,0.6431373,0.345098,1)
		_OutlineMultiplier("OutlineMultiplier", Float) = 0.5
		_OutlineSaturation("OutlineSaturation", Range( 0 , 3)) = 0.5
		_zOffset("zOffset", Float) = 0
		_zFactor("zFactor", Float) = 0
		_width("width", Float) = 1
		_StencilRef("StencilRef", Float) = 128
		_FogColor("FogColor", Color) = (0,0,0,0)
		_ColorOffset("ColorOffset", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Pass
		{
			ColorMask 0
			ZWrite On
		}

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZWrite On
		Offset  [_zFactor] , [_zOffset]
		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilRef]
			WriteMask [_StencilRef]
			Comp NotEqual
			Pass Keep
		}
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _zOffset;
		uniform float _zFactor;
		uniform float _StencilRef;
		uniform float _width;
		uniform float _ColorOffset;
		uniform float4 _Color4;
		uniform float4 _Color3;
		uniform float4 _Color2;
		uniform float4 _Color1;
		uniform float4 _Color0;
		uniform float _OutlineMultiplier;
		uniform float _OutlineSaturation;
		uniform float4 _FogColor;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertexNormal = v.normal.xyz;
			float3 normalizeResult153 = normalize( ase_vertexNormal );
			v.vertex.xyz += ( normalizeResult153 * _width );
			v.vertex.w = 1;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			int temp_output_14_0_g548 = (int)trunc( ( frac( ( ( _ColorOffset * 0.2 ) + i.uv_texcoord.x ) ) * 5.0 ) );
			int ColorIDInt16_g548 = temp_output_14_0_g548;
			float3 ifLocalVar22_g548 = 0;
			UNITY_BRANCH 
			if( ColorIDInt16_g548 > 4.0 )
				ifLocalVar22_g548 = float3( 0,0,1 );
			else if( ColorIDInt16_g548 == 4.0 )
				ifLocalVar22_g548 = _Color4.rgb;
			float3 ifLocalVar18_g548 = 0;
			UNITY_BRANCH 
			if( ColorIDInt16_g548 == 1.0 )
				ifLocalVar18_g548 = _Color1.rgb;
			else if( ColorIDInt16_g548 < 1.0 )
				ifLocalVar18_g548 = _Color0.rgb;
			float3 ifLocalVar15_g548 = 0;
			UNITY_BRANCH 
			if( ColorIDInt16_g548 == 2.0 )
				ifLocalVar15_g548 = _Color2.rgb;
			else if( ColorIDInt16_g548 < 2.0 )
				ifLocalVar15_g548 = ifLocalVar18_g548;
			float3 ifLocalVar13_g548 = 0;
			UNITY_BRANCH 
			if( temp_output_14_0_g548 > 3.0 )
				ifLocalVar13_g548 = ifLocalVar22_g548;
			else if( temp_output_14_0_g548 == 3.0 )
				ifLocalVar13_g548 = _Color3.rgb;
			else if( temp_output_14_0_g548 < 3.0 )
				ifLocalVar13_g548 = ifLocalVar15_g548;
			float3 temp_output_46_0 = ifLocalVar13_g548;
			float3 BaseColor71 = temp_output_46_0;
			float3 desaturateInitialColor130 = ( BaseColor71 * _OutlineMultiplier );
			float desaturateDot130 = dot( desaturateInitialColor130, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar130 = lerp( desaturateInitialColor130, desaturateDot130.xxx, ( 1.0 - _OutlineSaturation ) );
			float3 lerpResult158 = lerp( desaturateVar130 , (_FogColor).rgb , _FogColor.a);
			o.Emission = lerpResult158;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19108
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-847.1083,3.145679;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TruncOpNode;48;-628.9409,14.86306;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;46;-466.9407,16.86334;Inherit;True;mf_colorIDswitch;-1;;548;b1c9f9dbe40ac084aa2ab2372bd5fafa;0;7;14;INT;0;False;20;FLOAT3;0.745283,0.4324048,0.5106243;False;3;FLOAT3;0.02352939,0.3465245,0.5960785;False;4;FLOAT3;0.7529412,0.6463467,0.2666667;False;5;FLOAT3;0.2208971,0.6415094,0.3449238;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;54;-794.2657,397.1429;Inherit;False;Property;_Color1;Color 1;4;0;Create;True;0;0;0;False;0;False;0.02352941,0.345098,0.5960785,1;0,0.3398693,0.8823529,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;55;-799.2657,564.1428;Inherit;False;Property;_Color2;Color 2;5;0;Create;True;0;0;0;False;0;False;0.7529413,0.6470588,0.2666667,1;1,0.9215686,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;51;-797.8594,730.1429;Inherit;False;Property;_Color3;Color 3;6;0;Create;True;0;0;0;False;0;False;0.7058824,0.3019608,0.1568628,1;0.972549,0.2741413,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;52;-804.3907,897.2053;Inherit;False;Property;_Color4;Color 4;7;0;Create;True;0;0;0;False;0;False;0.2196079,0.6431373,0.345098,1;0.173436,0.596,0.2016069,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-218.4808,247.9455;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;58;-466.4625,426.2684;Inherit;False;Property;_DarkenMulti;DarkenMulti;14;0;Create;True;0;0;0;False;0;False;1;0.82;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;71;-99.57587,10.73273;Inherit;False;BaseColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;77;-841.5291,-1217.321;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;60;-670.3453,-1212.353;Inherit;False;mf_antiAliasSDF;0;;549;ad50c8affd20c66448c00c6c93e5fce7;0;2;1;FLOAT;0;False;11;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;78;-894.5291,-800.321;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;67;-663.2466,-800.2242;Inherit;False;mf_antiAliasSDF;0;;550;ad50c8affd20c66448c00c6c93e5fce7;0;2;1;FLOAT;0;False;11;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;63;-231.1999,485.6923;Inherit;True;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;88;-217.4373,714.8777;Inherit;True;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;3;-1050.178,-1027.993;Inherit;False;Property;_DarkenDir;DarkenDir;15;0;Create;True;0;0;0;False;0;False;0,1,0;0,-1,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;76;752.8088,-299.3331;Inherit;False;73;LightenColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;5;533.7444,-356.0406;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;87;265.8636,-346.8534;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NormalVertexDataNode;4;-1361.444,-1272.813;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TransformDirectionNode;107;-1113.649,-1266.032;Inherit;False;Object;World;False;Fast;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NormalVertexDataNode;69;-1451.937,-813.034;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;79;-1461.689,-441.5013;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TransformDirectionNode;108;-1157.27,-796.3428;Inherit;False;Object;World;False;Fast;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector3Node;80;-1130.269,-254.0225;Inherit;False;Property;_SidesDir;SidesDir;2;0;Create;True;0;0;0;False;0;False;0,1,0;-1,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;85;-467.1771,-432.4291;Inherit;True;mf_antiAliasSDF;0;;552;ad50c8affd20c66448c00c6c93e5fce7;0;2;1;FLOAT;0;False;11;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;81;-969.0211,-432.8749;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;-1,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;84;-780.1771,-436.4291;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;91;47.88429,-305.5556;Inherit;False;90;SidesColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;74;-97.62928,-425.5487;Inherit;False;71;BaseColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;118;-440.1993,-1551.374;Inherit;False;StepSoftness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;336.014,-117.6051;Inherit;False;72;DarkenColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;119;-519.1061,-546.782;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;120;-711.0734,-484.3632;Inherit;False;118;StepSoftness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;121;-536.7719,-616.2673;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;116;-388.3799,-599.7791;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;122;-433.4272,-760.2436;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;123;-625.3948,-697.8248;Inherit;False;118;StepSoftness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;124;-451.093,-829.7289;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;125;-302.701,-813.2407;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;126;-444.6066,-1137.161;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;127;-636.5742,-1074.742;Inherit;False;118;StepSoftness;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;128;-462.2724,-1206.646;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;129;-313.8804,-1190.158;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;57;337.7956,-475.8529;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;64;-435.9444,557.6882;Inherit;False;Property;_LightenPower;LightenPower;10;0;Create;True;0;0;0;False;0;False;0.38;0.59;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;89;-438.6156,738.7552;Inherit;False;Property;_SidePower;SidePower;18;0;Create;True;0;0;0;False;0;False;0.38;1.36;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;50;-800.2657,223.5257;Inherit;False;Property;_Color0;Color 0;3;1;[Header];Create;True;1;Colors;0;0;False;0;False;0.7450981,0.4313726,0.509804,1;0.9764706,0,0.2983658,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;117;-609.7905,-1557.262;Inherit;False;Property;_StepSoftness;StepSoftness;8;1;[Header];Create;True;1;Global;0;0;False;0;False;0;0.19;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;61;-827.3452,-1103.352;Inherit;False;Property;_DarkenStep;DarkenStep;13;1;[Header];Create;True;1;Shadows;0;0;False;0;False;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;68;-853.2465,-587.2233;Inherit;False;Property;_LightenStep;LightenStep;9;1;[Header];Create;True;1;Highlights;0;0;False;0;False;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-706.7119,-548.6038;Inherit;False;Property;_SidesStep;SidesStep;17;1;[Header];Create;True;1;Sides;0;0;False;0;False;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;70;-1119.777,-619.4686;Inherit;False;Property;_LightenDir;LightenDir;11;1;[Header];Create;True;0;0;0;False;0;False;0,1,0;0,1,0.02;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;113;907.2292,36.60242;Inherit;False;Property;_OutlineWidth;OutlineWidth;20;1;[Header];Create;True;1;Outlines;0;0;False;0;False;0;0.11;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;114;712.2292,-69.39758;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;112;490.2292,-70.39758;Inherit;False;71;BaseColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;115;464.2292,-0.3975792;Inherit;False;Property;_OutlineMultiplier;OutlineMultiplier;21;0;Create;True;0;0;0;False;0;False;0.5;0.47;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;130;845.4809,-72.14099;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;132;665.4809,87.85901;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;90;421.2697,705.0117;Inherit;False;SidesColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;134;234.8035,848.6529;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;135;243.2386,702.9856;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;461.0172,476.5016;Inherit;False;LightenColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;72;501.6113,249.1156;Inherit;False;DarkenColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;137;254.7703,611.8455;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;138;263.2055,466.1781;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;139;270.3445,383.4244;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;140;278.7797,237.7572;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;133;40.82328,850.8311;Inherit;False;Property;_SidesSaturation;SidesSaturation;19;0;Create;True;0;0;0;False;0;False;0.5;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;136;60.79015,612.8256;Inherit;False;Property;_LightenSaturation;LightenSaturation;12;0;Create;True;0;0;0;False;0;False;0.5;0.75;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;141;76.36435,384.4046;Inherit;False;Property;_DarkenSaturation;DarkenSaturation;16;0;Create;True;0;0;0;False;0;False;0.5;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;131;343.3118,88.58731;Inherit;False;Property;_OutlineSaturation;OutlineSaturation;22;0;Create;True;0;0;0;False;0;False;0.5;0.66;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;65;1023.116,-407.5912;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TransformDirectionNode;109;-1206.378,-439.2079;Inherit;False;Object;World;False;Fast;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;146;1226.267,461.3919;Inherit;False;Property;_zOffset;zOffset;23;0;Create;True;0;0;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;147;1224.545,390.3919;Inherit;False;Property;_zFactor;zFactor;24;0;Create;True;0;0;0;True;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;153;1248.75,-60.06506;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;150;1437.217,-48.98145;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;152;1252.217,22.01855;Inherit;False;Property;_width;width;25;0;Create;True;0;0;0;False;0;False;1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;155;1618.75,239.9349;Inherit;False;Property;_StencilRef;StencilRef;26;0;Create;True;0;0;0;True;0;False;128;128;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;149;1259.963,136.5381;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;148;1012.633,174.1256;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2004.246,-322.8826;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;splashanim_blocks_outline;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;1;False;;0;False;;True;0;True;_zFactor;0;True;_zOffset;True;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;12;all;True;True;True;True;0;False;;True;128;True;_StencilRef;128;True;_StencilRef;128;True;_StencilRef;6;False;;1;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;True;2;5;False;;10;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.ColorNode;156;1324.949,-390.5898;Inherit;False;Property;_FogColor;FogColor;27;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.4431373,0.7607844,0.9803922,0.3411765;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;157;1511.9,-390.59;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;158;1775.68,-449.4925;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;159;1698.218,-283.6628;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;160;-1718.475,325.0121;Inherit;True;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;161;-1453.147,225.6648;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;163;-1221.344,330.8504;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;162;-1828.716,139.6685;Inherit;False;Property;_ColorOffset;ColorOffset;28;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;164;-1589.581,83.23358;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.2;False;1;FLOAT;0
WireConnection;30;0;163;0
WireConnection;48;0;30;0
WireConnection;46;14;48;0
WireConnection;46;20;50;0
WireConnection;46;3;54;0
WireConnection;46;4;55;0
WireConnection;46;5;51;0
WireConnection;46;6;52;0
WireConnection;56;0;46;0
WireConnection;56;1;58;0
WireConnection;71;0;46;0
WireConnection;77;0;4;0
WireConnection;77;1;3;0
WireConnection;60;1;77;0
WireConnection;60;11;61;0
WireConnection;78;0;69;0
WireConnection;78;1;70;0
WireConnection;67;1;78;0
WireConnection;67;11;68;0
WireConnection;63;0;46;0
WireConnection;63;1;64;0
WireConnection;88;0;46;0
WireConnection;88;1;89;0
WireConnection;5;0;87;0
WireConnection;5;1;75;0
WireConnection;5;2;57;0
WireConnection;87;0;74;0
WireConnection;87;1;91;0
WireConnection;87;2;116;0
WireConnection;107;0;4;0
WireConnection;108;0;69;0
WireConnection;85;1;84;0
WireConnection;85;11;83;0
WireConnection;81;0;79;0
WireConnection;84;0;81;0
WireConnection;118;0;117;0
WireConnection;119;0;83;0
WireConnection;119;1;120;0
WireConnection;121;0;83;0
WireConnection;121;1;120;0
WireConnection;116;0;84;0
WireConnection;116;1;121;0
WireConnection;116;2;119;0
WireConnection;122;0;68;0
WireConnection;122;1;123;0
WireConnection;124;0;68;0
WireConnection;124;1;123;0
WireConnection;125;0;78;0
WireConnection;125;1;124;0
WireConnection;125;2;122;0
WireConnection;126;0;61;0
WireConnection;126;1;127;0
WireConnection;128;0;61;0
WireConnection;128;1;127;0
WireConnection;129;0;77;0
WireConnection;129;1;128;0
WireConnection;129;2;126;0
WireConnection;57;0;129;0
WireConnection;114;0;112;0
WireConnection;114;1;115;0
WireConnection;130;0;114;0
WireConnection;130;1;132;0
WireConnection;132;0;131;0
WireConnection;90;0;135;0
WireConnection;134;0;133;0
WireConnection;135;0;88;0
WireConnection;135;1;134;0
WireConnection;73;0;138;0
WireConnection;72;0;140;0
WireConnection;137;0;136;0
WireConnection;138;0;63;0
WireConnection;138;1;137;0
WireConnection;139;0;141;0
WireConnection;140;0;56;0
WireConnection;140;1;139;0
WireConnection;65;0;5;0
WireConnection;65;1;76;0
WireConnection;65;2;125;0
WireConnection;109;0;79;0
WireConnection;153;0;148;0
WireConnection;150;0;153;0
WireConnection;150;1;152;0
WireConnection;0;2;158;0
WireConnection;0;11;150;0
WireConnection;157;0;156;0
WireConnection;158;0;130;0
WireConnection;158;1;157;0
WireConnection;158;2;159;0
WireConnection;159;0;156;4
WireConnection;161;0;164;0
WireConnection;161;1;160;1
WireConnection;163;0;161;0
WireConnection;164;0;162;0
ASEEND*/
//CHKSM=91FA9FC5B5031A412333D1F8C611175B73EEF843