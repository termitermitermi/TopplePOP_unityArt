// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/StoryEnvironment"
{
	Properties
	{
		[NoScaleOffset]_MainTex("Albedo", 2D) = "white" {}
		_Color("Color", Color) = (0.5,0.5,0.5,1)
		[Toggle(_USEUV1ALBEDO_ON)] _UseUV1Albedo("UseUV1Albedo", Float) = 0
		_AlbedoTiling("AlbedoTiling", Vector) = (1,1,0,0)
		_AlbedoOffset("AlbedoOffset", Vector) = (0,0,0,0)
		[HideInInspector]_ToonRamp("ToonRamp", 2D) = "white" {}
		_DetailTex("DetailMap", 2D) = "gray" {}
		[NoScaleOffset]_BigTex("AO", 2D) = "white" {}
		_AOAmount("AO Amount", Range( 0 , 5)) = 0
		[Toggle(_USEFOG_ON)] _UseFog("UseFog", Float) = 1
		_FogLow("FogLow", Float) = 0
		_FogHigh("FogHigh", Float) = 40
		_FogNear("FogNear", Float) = 0
		_FogCutoutNear("FogCutoutNear", Float) = 0
		_FogFar("FogFar", Float) = 100
		_FogCutoutFar("FogCutoutFar", Float) = 0.5
		_FogLowNearColor("FogLowNearColor", Color) = (1,0,0,1)
		_FogLowFarColor("FogLowFarColor", Color) = (0,0.213161,1,1)
		_FogHighNearColor("FogHighNearColor", Color) = (1,0,0.9933062,1)
		_FogHighFarColor("FogHighFarColor", Color) = (0,1,0.04610038,1)
		_FogAmount("FogAmount", Range( 0 , 1)) = 0.5
		_NRM("NRM", 2D) = "bump" {}
		[NoScaleOffset]_SmothMetalEmitMap("SmothMetalEmitMap", 2D) = "black" {}
		_Fresnel2Color("Fresnel2Color", Color) = (0,0,0,0)
		_EmissiveColor("EmissiveColor", Color) = (0,0,0,0)
		[Toggle(_FRESNEL1_ON)] _Fresnel1("Fresnel1", Float) = 0
		_Fresnel1Multiplier("Fresnel1Multiplier", Float) = 0
		_Fresnel1Dir("Fresnel1Dir", Vector) = (0,0,0,0)
		_Fresnel1Power("Fresnel1Power", Float) = 0
		[Toggle(_FRESNEL2_ON)] _Fresnel2("Fresnel2", Float) = 0
		_Fresnel2Multiplier("Fresnel2Multiplier", Float) = 0
		_Fresnel2Dir("Fresnel2Dir", Vector) = (0,0,0,0)
		_Fresnel2Power("Fresnel2Power", Float) = 0
		[Toggle(_UNLIT_ON)] _Unlit("Unlit", Float) = 0
		[Toggle(_WATERLINE_ON)] _WaterLine("WaterLine", Float) = 0
		_minz("minz", Float) = 0
		_RotAngle("RotAngle", Float) = 0
		_WaterEdgeColor("WaterEdgeColor", Color) = (0,0,0,0)
		_softness("softness", Float) = 0
		_Offset("Offset", Float) = 0
		[Toggle(_USEFAKEEMISSIVE_ON)] _UseFakeEmissive("UseFakeEmissive", Float) = 0
		_FakeEmissiveCutoffMax("FakeEmissiveCutoffMax", Float) = 0.61
		_FakeEmissiveAmt("FakeEmissiveAmt", Float) = 0
		_FakeEmissiveCutoffMin("FakeEmissiveCutoffMin", Float) = 0.61
		_EdgeDarkenRange("EdgeDarkenRange", Vector) = (0.3,0.5,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 4.0
		#pragma multi_compile_local __ _USEFOG_ON
		#pragma multi_compile_local __ _USEFAKEEMISSIVE_ON
		#pragma multi_compile_local __ _FRESNEL2_ON
		#pragma shader_feature_local _FRESNEL1_ON
		#pragma multi_compile_local __ _USEUV1ALBEDO_ON
		#pragma multi_compile_local __ _UNLIT_ON
		#pragma multi_compile_local __ _WATERLINE_ON
		#pragma surface surf StandardCustomLighting keepalpha noshadow exclude_path:deferred nolightmap  nodynlightmap nodirlightmap nofog 
		struct Input
		{
			float3 worldPos;
			half3 worldNormal;
			INTERNAL_DATA
			float2 uv2_texcoord2;
			float2 uv_texcoord;
			float4 screenPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform half3 _Fresnel1Dir;
		uniform half _Fresnel1Power;
		uniform half _Fresnel1Multiplier;
		uniform sampler2D _SmothMetalEmitMap;
		uniform half2 _AlbedoTiling;
		uniform half2 _AlbedoOffset;
		uniform half4 _EmissiveColor;
		uniform half4 _Fresnel2Color;
		uniform half3 _Fresnel2Dir;
		uniform half _Fresnel2Power;
		uniform half _Fresnel2Multiplier;
		uniform sampler2D _MainTex;
		uniform half _FakeEmissiveCutoffMin;
		uniform half _FakeEmissiveCutoffMax;
		uniform half _FakeEmissiveAmt;
		uniform half4 _FogLowNearColor;
		uniform half4 _FogLowFarColor;
		uniform half _FogNear;
		uniform half _FogFar;
		uniform half4 _FogHighNearColor;
		uniform half4 _FogHighFarColor;
		uniform half _FogLow;
		uniform half _FogHigh;
		uniform half _FogCutoutNear;
		uniform half _FogCutoutFar;
		uniform half4 _VignetteColor;
		uniform half _VignetteSize;
		uniform half _VignetteIntensity;
		uniform half _VignetteLerpPower;
		uniform half _FogAmount;
		uniform half2 _EdgeDarkenRange;
		uniform half4 _Color;
		uniform sampler2D _DetailTex;
		SamplerState sampler_DetailTex;
		uniform half4 _DetailTex_ST;
		uniform half4 _WaterEdgeColor;
		uniform half _minz;
		uniform half _Offset;
		uniform half _softness;
		uniform half _RotAngle;
		uniform sampler2D _ToonRamp;
		uniform sampler2D _NRM;
		uniform half4 _NRM_ST;
		uniform sampler2D _BigTex;
		SamplerState sampler_BigTex;
		uniform half _AOAmount;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			half smoothstepResult325 = smoothstep( _EdgeDarkenRange.x , _EdgeDarkenRange.y , ( 1.0 - distance( abs( (ase_screenPosNorm).x ) , 0.5 ) ));
			half DarkEdge327 = smoothstepResult325;
			float2 uv2_TexCoord227 = i.uv2_texcoord2 * _AlbedoTiling + _AlbedoOffset;
			float2 uv_TexCoord226 = i.uv_texcoord * _AlbedoTiling + _AlbedoOffset;
			#ifdef _USEUV1ALBEDO_ON
				half2 staticSwitch228 = uv_TexCoord226;
			#else
				half2 staticSwitch228 = uv2_TexCoord227;
			#endif
			half4 tex2DNode1 = tex2D( _MainTex, staticSwitch228 );
			half4 temp_output_96_0 = ( tex2DNode1 * _Color );
			float2 uv_DetailTex = i.uv_texcoord * _DetailTex_ST.xy + _DetailTex_ST.zw;
			half4 tex2DNode4 = tex2D( _DetailTex, uv_DetailTex );
			half4 lerpResult179 = lerp( temp_output_96_0 , ( ( tex2DNode4.r + 0.75 ) * temp_output_96_0 ) , tex2DNode4.r);
			half temp_output_15_0_g44 = ( _minz + _Offset );
			float3 ase_worldPos = i.worldPos;
			half2 appendResult3_g44 = (half2(ase_worldPos.z , ase_worldPos.y));
			float cos1_g44 = cos( _RotAngle );
			float sin1_g44 = sin( _RotAngle );
			half2 rotator1_g44 = mul( appendResult3_g44 - float2( 0,0 ) , float2x2( cos1_g44 , -sin1_g44 , sin1_g44 , cos1_g44 )) + float2( 0,0 );
			half smoothstepResult8_g44 = smoothstep( ( temp_output_15_0_g44 + ( 1.0 - _softness ) ) , ( temp_output_15_0_g44 + _softness ) , rotator1_g44.x);
			half4 lerpResult14_g44 = lerp( lerpResult179 , _WaterEdgeColor , smoothstepResult8_g44);
			half4 temp_output_287_0 = lerpResult14_g44;
			#ifdef _WATERLINE_ON
				half4 staticSwitch288 = temp_output_287_0;
			#else
				half4 staticSwitch288 = lerpResult179;
			#endif
			half4 temp_output_1_0_g46 = staticSwitch288;
			half2 VignetteCoord7_g46 = ( ( (ase_screenPosNorm).xy / ase_screenPosNorm.w ) - float2( 0.5,0.5 ) );
			half dotResult9_g46 = dot( VignetteCoord7_g46 , VignetteCoord7_g46 );
			half Vignette_rf13_g46 = ( sqrt( dotResult9_g46 ) * _VignetteSize );
			half Vignette_rf_2_117_g46 = ( ( Vignette_rf13_g46 * Vignette_rf13_g46 ) + 1.0 );
			half Vignette_e22_g46 = ( 1.0 / pow( Vignette_rf_2_117_g46 , _VignetteIntensity ) );
			half temp_output_29_0_g46 = pow( ( _VignetteColor.a * ( 1.0 - Vignette_e22_g46 ) ) , _VignetteLerpPower );
			half4 lerpResult35_g46 = lerp( temp_output_1_0_g46 , half4( ( (temp_output_1_0_g46).rgb * (_VignetteColor).rgb ) , 0.0 ) , temp_output_29_0_g46);
			half4 temp_output_296_0 = saturate( lerpResult35_g46 );
			half3 _Vector0 = half3(0,0,1);
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			half3 ase_worldlightDir = 0;
			#else //aseld
			half3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			half dotResult3_g47 = dot( (WorldNormalVector( i , _Vector0 )) , ase_worldlightDir );
			half2 temp_cast_9 = (dotResult3_g47).xx;
			half4 temp_cast_10 = (dotResult3_g47).xxxx;
			half4 lerpResult31_g47 = lerp( tex2D( _ToonRamp, temp_cast_9 ) , temp_cast_10 , float4( 1,1,1,0 ));
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			half4 ase_lightColor = 0;
			#else //aselc
			half4 ase_lightColor = _LightColor0;
			#endif //aselc
			float2 uv_NRM = i.uv_texcoord * _NRM_ST.xy + _NRM_ST.zw;
			half3 temp_output_23_0_g47 = UnpackNormal( tex2D( _NRM, uv_NRM ) );
			UnityGI gi13_g47 = gi;
			float3 diffNorm13_g47 = WorldNormalVector( i , temp_output_23_0_g47 );
			gi13_g47 = UnityGI_Base( data, 1, diffNorm13_g47 );
			half3 indirectDiffuse13_g47 = gi13_g47.indirect.diffuse + diffNorm13_g47 * 0.0001;
			float3 indirectNormal15_g47 = WorldNormalVector( i , temp_output_23_0_g47 );
			float2 uv_BigTex3 = i.uv_texcoord;
			half lerpResult13 = lerp( 1.0 , tex2D( _BigTex, uv_BigTex3 ).r , _AOAmount);
			half AO206 = lerpResult13;
			Unity_GlossyEnvironmentData g15_g47 = UnityGlossyEnvironmentSetup( 0.0, data.worldViewDir, indirectNormal15_g47, float3(0,0,0));
			half3 indirectSpecular15_g47 = UnityGI_IndirectSpecular( data, AO206, indirectNormal15_g47, g15_g47 );
			half4 temp_output_272_0 = ( temp_output_296_0 * ( ( lerpResult31_g47 * half4( ( ase_lightColor.rgb * ase_lightAtten ) , 0.0 ) * unity_AmbientSky ) + half4( indirectDiffuse13_g47 , 0.0 ) + half4( indirectSpecular15_g47 , 0.0 ) ) );
			half4 WaterLineRGBA290 = temp_output_287_0;
			half4 lerpResult291 = lerp( temp_output_272_0 , half4( ( (WaterLineRGBA290).rgb * DarkEdge327 ) , 0.0 ) , (WaterLineRGBA290).a);
			#ifdef _WATERLINE_ON
				half4 staticSwitch289 = lerpResult291;
			#else
				half4 staticSwitch289 = temp_output_272_0;
			#endif
			#ifdef _UNLIT_ON
				half4 staticSwitch280 = temp_output_296_0;
			#else
				half4 staticSwitch280 = staticSwitch289;
			#endif
			half4 Albedo299 = tex2DNode1;
			half4 temp_cast_15 = (_FakeEmissiveCutoffMin).xxxx;
			half4 temp_cast_16 = (_FakeEmissiveCutoffMax).xxxx;
			half4 smoothstepResult306 = smoothstep( temp_cast_15 , temp_cast_16 , Albedo299);
			half4 FakeEmissiveAlpha310 = ( smoothstepResult306 * _FakeEmissiveAmt );
			half4 lerpResult316 = lerp( staticSwitch280 , Albedo299 , FakeEmissiveAlpha310);
			#ifdef _USEFAKEEMISSIVE_ON
				half4 staticSwitch315 = lerpResult316;
			#else
				half4 staticSwitch315 = staticSwitch280;
			#endif
			half4 temp_output_337_0 = ( DarkEdge327 * staticSwitch315 );
			half depthFromOrigin117 = ase_worldPos.z;
			half smoothstepResult122 = smoothstep( _FogNear , _FogFar , depthFromOrigin117);
			half4 lerpResult121 = lerp( _FogLowNearColor , _FogLowFarColor , smoothstepResult122);
			half4 fogColorLow119 = lerpResult121;
			half smoothstepResult129 = smoothstep( _FogNear , _FogFar , depthFromOrigin117);
			half4 lerpResult127 = lerp( _FogHighNearColor , _FogHighFarColor , smoothstepResult129);
			half4 fogColorHigh120 = lerpResult127;
			half smoothstepResult132 = smoothstep( ( 0.0 + _FogLow ) , ( 0.0 + _FogHigh ) , ase_screenPosNorm.y);
			half4 lerpResult131 = lerp( fogColorLow119 , fogColorHigh120 , smoothstepResult132);
			half smoothstepResult138 = smoothstep( _FogCutoutNear , _FogCutoutFar , depthFromOrigin117);
			half lerpResult135 = lerp( 0.0 , (lerpResult131).a , smoothstepResult138);
			half4 appendResult140 = (half4((lerpResult131).rgb , lerpResult135));
			half VigFinal297 = saturate( temp_output_29_0_g46 );
			half4 lerpResult276 = lerp( appendResult140 , float4( 0,0,0,0 ) , VigFinal297);
			half4 fogColor141 = lerpResult276;
			half temp_output_145_0 = ( (fogColor141).w * _FogAmount );
			half4 lerpResult340 = lerp( temp_output_337_0 , fogColor141 , temp_output_145_0);
			#ifdef _USEFOG_ON
				half4 staticSwitch339 = lerpResult340;
			#else
				half4 staticSwitch339 = temp_output_337_0;
			#endif
			c.rgb = staticSwitch339.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			half3 ase_worldNormal = WorldNormalVector( i, half3( 0, 0, 1 ) );
			half fresnelNdotV241 = dot( ase_worldNormal, _Fresnel1Dir );
			half fresnelNode241 = ( 0.0 + 1.0 * pow( max( 1.0 - fresnelNdotV241 , 0.0001 ), _Fresnel1Power ) );
			half clampResult248 = clamp( fresnelNode241 , 0.0 , 1.0 );
			#ifdef _FRESNEL1_ON
				half staticSwitch252 = ( clampResult248 * _Fresnel1Multiplier );
			#else
				half staticSwitch252 = 0.0;
			#endif
			half Fresnel1Mask256 = staticSwitch252;
			float2 uv2_TexCoord227 = i.uv2_texcoord2 * _AlbedoTiling + _AlbedoOffset;
			float2 uv_TexCoord226 = i.uv_texcoord * _AlbedoTiling + _AlbedoOffset;
			#ifdef _USEUV1ALBEDO_ON
				half2 staticSwitch228 = uv_TexCoord226;
			#else
				half2 staticSwitch228 = uv2_TexCoord227;
			#endif
			half4 SmothMetalEmitCondensed233 = tex2D( _SmothMetalEmitMap, staticSwitch228 );
			half4 temp_output_238_0 = saturate( ( ( ( Fresnel1Mask256 + (SmothMetalEmitCondensed233).b ) * _EmissiveColor ) * _EmissiveColor.a ) );
			half fresnelNdotV260 = dot( ase_worldNormal, _Fresnel2Dir );
			half fresnelNode260 = ( 0.0 + 1.0 * pow( max( 1.0 - fresnelNdotV260 , 0.0001 ), _Fresnel2Power ) );
			half clampResult262 = clamp( fresnelNode260 , 0.0 , 1.0 );
			#ifdef _FRESNEL1_ON
				half staticSwitch265 = ( clampResult262 * _Fresnel2Multiplier );
			#else
				half staticSwitch265 = 0.0;
			#endif
			half Fresnel2Mask266 = staticSwitch265;
			half4 lerpResult251 = lerp( temp_output_238_0 , ( _Fresnel2Color * Fresnel2Mask266 ) , Fresnel2Mask266);
			#ifdef _FRESNEL2_ON
				half4 staticSwitch270 = lerpResult251;
			#else
				half4 staticSwitch270 = temp_output_238_0;
			#endif
			half4 tex2DNode1 = tex2D( _MainTex, staticSwitch228 );
			half4 Albedo299 = tex2DNode1;
			half4 temp_cast_0 = (_FakeEmissiveCutoffMin).xxxx;
			half4 temp_cast_1 = (_FakeEmissiveCutoffMax).xxxx;
			half4 smoothstepResult306 = smoothstep( temp_cast_0 , temp_cast_1 , Albedo299);
			half4 FakeEmissiveAlpha310 = ( smoothstepResult306 * _FakeEmissiveAmt );
			half4 lerpResult311 = lerp( staticSwitch270 , Albedo299 , FakeEmissiveAlpha310);
			#ifdef _USEFAKEEMISSIVE_ON
				half4 staticSwitch300 = lerpResult311;
			#else
				half4 staticSwitch300 = staticSwitch270;
			#endif
			float3 ase_worldPos = i.worldPos;
			half depthFromOrigin117 = ase_worldPos.z;
			half smoothstepResult122 = smoothstep( _FogNear , _FogFar , depthFromOrigin117);
			half4 lerpResult121 = lerp( _FogLowNearColor , _FogLowFarColor , smoothstepResult122);
			half4 fogColorLow119 = lerpResult121;
			half smoothstepResult129 = smoothstep( _FogNear , _FogFar , depthFromOrigin117);
			half4 lerpResult127 = lerp( _FogHighNearColor , _FogHighFarColor , smoothstepResult129);
			half4 fogColorHigh120 = lerpResult127;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			half smoothstepResult132 = smoothstep( ( 0.0 + _FogLow ) , ( 0.0 + _FogHigh ) , ase_screenPosNorm.y);
			half4 lerpResult131 = lerp( fogColorLow119 , fogColorHigh120 , smoothstepResult132);
			half smoothstepResult138 = smoothstep( _FogCutoutNear , _FogCutoutFar , depthFromOrigin117);
			half lerpResult135 = lerp( 0.0 , (lerpResult131).a , smoothstepResult138);
			half4 appendResult140 = (half4((lerpResult131).rgb , lerpResult135));
			half2 VignetteCoord7_g46 = ( ( (ase_screenPosNorm).xy / ase_screenPosNorm.w ) - float2( 0.5,0.5 ) );
			half dotResult9_g46 = dot( VignetteCoord7_g46 , VignetteCoord7_g46 );
			half Vignette_rf13_g46 = ( sqrt( dotResult9_g46 ) * _VignetteSize );
			half Vignette_rf_2_117_g46 = ( ( Vignette_rf13_g46 * Vignette_rf13_g46 ) + 1.0 );
			half Vignette_e22_g46 = ( 1.0 / pow( Vignette_rf_2_117_g46 , _VignetteIntensity ) );
			half temp_output_29_0_g46 = pow( ( _VignetteColor.a * ( 1.0 - Vignette_e22_g46 ) ) , _VignetteLerpPower );
			half VigFinal297 = saturate( temp_output_29_0_g46 );
			half4 lerpResult276 = lerp( appendResult140 , float4( 0,0,0,0 ) , VigFinal297);
			half4 fogColor141 = lerpResult276;
			half temp_output_145_0 = ( (fogColor141).w * _FogAmount );
			half4 lerpResult142 = lerp( staticSwitch300 , fogColor141 , temp_output_145_0);
			#ifdef _USEFOG_ON
				half4 staticSwitch279 = lerpResult142;
			#else
				half4 staticSwitch279 = staticSwitch300;
			#endif
			o.Albedo = staticSwitch279.rgb;
			half smoothstepResult325 = smoothstep( _EdgeDarkenRange.x , _EdgeDarkenRange.y , ( 1.0 - distance( abs( (ase_screenPosNorm).x ) , 0.5 ) ));
			half DarkEdge327 = smoothstepResult325;
			o.Emission = ( DarkEdge327 * staticSwitch270 ).rgb;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18400
0;0;1920;1011;-3860.017;925.1345;1.374928;True;False
Node;AmplifyShaderEditor.Vector2Node;298;-1828.092,-479.9275;Inherit;False;Property;_AlbedoOffset;AlbedoOffset;4;0;Create;True;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;231;-1869.975,-635.3093;Inherit;False;Property;_AlbedoTiling;AlbedoTiling;3;0;Create;True;0;0;False;0;False;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;227;-1370.023,-690.1292;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;226;-1390.023,-542.1292;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;154;1579.356,-3341.663;Inherit;False;2130.826;1375.951;Comment;39;118;117;104;103;128;101;102;126;108;105;149;148;107;129;122;109;127;121;119;120;131;112;111;139;136;138;137;135;140;141;124;132;114;160;161;162;163;276;278;Build Fog Matrix;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;254;2819.244,-1908.282;Inherit;False;1137.001;429.0005;Fresnel1;4;241;243;244;256;;1,1,1,1;0;0
Node;AmplifyShaderEditor.StaticSwitch;228;-1046.023,-715.1292;Inherit;False;Property;_UseUV1Albedo;UseUV1Albedo;2;0;Create;True;0;0;True;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WorldPosInputsNode;118;1997.829,-3244.963;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;244;2872.244,-1717.282;Inherit;False;Property;_Fresnel1Power;Fresnel1Power;30;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;243;2869.244,-1858.282;Inherit;False;Property;_Fresnel1Dir;Fresnel1Dir;29;0;Create;True;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;1;-198.7001,-803.8;Inherit;True;Property;_MainTex;Albedo;0;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;e87c3cdd775f0584fb996376e5f1d2bb;e87c3cdd775f0584fb996376e5f1d2bb;True;1;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;98;-190.285,-603.0076;Inherit;False;Property;_Color;Color;1;0;Create;True;0;0;False;0;False;0.5,0.5,0.5,1;0.735849,0.735849,0.735849,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenPosInputsNode;319;463.1721,-1487.447;Float;True;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-273.0786,-997.9298;Inherit;True;Property;_DetailTex;DetailMap;7;0;Create;False;0;0;False;0;False;-1;f317a5bebd2b0ac4c96e4f7c1b59a397;6d9c5e59743d54849879c1ee15068aa0;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;320;689.2762,-1516.309;Inherit;False;True;False;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;241;3087.244,-1814.282;Inherit;False;Standard;WorldNormal;ViewDir;False;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;170;182.1086,-1002.456;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.75;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;117;1980.764,-3107.009;Inherit;False;depthFromOrigin;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;232.9968,-773.859;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.AbsOpNode;321;975.9941,-1484.452;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;167;367.0437,-1003.354;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;126;1629.356,-2656.128;Inherit;False;117;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;250;3278.245,-1641.282;Inherit;False;Property;_Fresnel1Multiplier;Fresnel1Multiplier;28;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;102;1682.511,-2521.752;Inherit;False;Property;_FogFar;FogFar;15;0;Create;True;0;0;False;0;False;100;26.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;248;3337.245,-1764.282;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;267;2835.926,-1420.864;Inherit;False;1168.427;382;Comment;9;258;259;260;261;262;263;264;265;266;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;128;1686.47,-2080.712;Inherit;False;117;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;101;1684.341,-2590.032;Inherit;False;Property;_FogNear;FogNear;13;0;Create;True;0;0;False;0;False;0;3.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;179;644.527,-892.6914;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;108;1631.347,-2443.356;Inherit;False;Property;_FogHighNearColor;FogHighNearColor;19;0;Create;True;0;0;False;0;False;1,0,0.9933062,1;0.547156,0.56,0.5445871,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;103;2160.626,-2498.115;Inherit;False;Property;_FogHigh;FogHigh;12;0;Create;True;0;0;False;0;False;40;0.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;107;1633.209,-2822.936;Inherit;False;Property;_FogLowFarColor;FogLowFarColor;18;0;Create;True;0;0;False;0;False;0,0.213161,1,1;0.4892364,0.496,0.4396363,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;109;1630.955,-2277.27;Inherit;False;Property;_FogHighFarColor;FogHighFarColor;20;0;Create;True;0;0;False;0;False;0,1,0.04610038,1;0.5490196,0.5607843,0.5450981,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;129;1889.165,-2281.926;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;104;2148.659,-2647.919;Inherit;False;Property;_FogLow;FogLow;11;0;Create;True;0;0;False;0;False;0;0.41;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;105;1633.276,-2995.384;Inherit;False;Property;_FogLowNearColor;FogLowNearColor;17;0;Create;True;0;0;False;0;False;1,0,0,1;0.8352942,0.854902,0.8313726,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;232;-725.986,-403.3121;Inherit;True;Property;_SmothMetalEmitMap;SmothMetalEmitMap;24;1;[NoScaleOffset];Create;True;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;259;2885.926,-1370.864;Inherit;False;Property;_Fresnel2Dir;Fresnel2Dir;33;0;Create;True;0;0;False;0;False;0,0,0;1,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;258;2888.926,-1229.864;Inherit;False;Property;_Fresnel2Power;Fresnel2Power;34;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;253;3475.124,-1820.218;Inherit;False;Constant;_zero;zero;27;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;249;3497.245,-1683.282;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;322;1142.999,-1498.595;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;122;1921.517,-2714.42;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-157.3619,-374.6019;Inherit;True;Property;_BigTex;AO;8;1;[NoScaleOffset];Create;False;0;0;False;0;False;-1;389d11faabdbcfa4f837b8e5d7466375;389d11faabdbcfa4f837b8e5d7466375;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;323;1350.347,-1503.627;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;324;1307.393,-1281.971;Inherit;False;Property;_EdgeDarkenRange;EdgeDarkenRange;47;0;Create;True;0;0;False;0;False;0.3,0.5;0.25,0.64;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;7;-140.777,-187.4397;Inherit;False;Property;_AOAmount;AO Amount;9;0;Create;False;0;0;False;0;False;0;1;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;287;852.0192,-779.1143;Inherit;False;waterlineColorReplace;37;;44;aca7f94936d373143abcd863df445b82;0;1;18;FLOAT4;0,0,0,0;False;2;FLOAT;19;COLOR;0
Node;AmplifyShaderEditor.LerpOp;127;2133.734,-2388.433;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;149;2323.613,-2536.69;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;252;3636.124,-1816.218;Inherit;False;Property;_Fresnel1;Fresnel1;27;0;Create;True;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;260;3103.926,-1326.864;Inherit;False;Standard;WorldNormal;ViewDir;False;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;233;-433.986,-418.3121;Inherit;False;SmothMetalEmitCondensed;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;148;2325.613,-2670.69;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;121;2124.729,-2837.055;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;163;2344.177,-3036.348;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;261;3294.927,-1153.864;Inherit;False;Property;_Fresnel2Multiplier;Fresnel2Multiplier;32;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;262;3353.927,-1276.864;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;256;3763.671,-1688.729;Inherit;False;Fresnel1Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;234;2583.31,-850.7728;Inherit;False;233;SmothMetalEmitCondensed;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;119;2284.729,-2842.055;Inherit;False;fogColorLow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;120;2293.482,-2394.322;Inherit;False;fogColorHigh;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;132;2510.878,-2661.257;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;13;295.8481,-359.5991;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;325;1569.393,-1503.971;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;290;1174.676,-701.0714;Inherit;False;WaterLineRGBA;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;206;499.5735,-346.4188;Inherit;False;AO;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;299;163.8961,-849.4773;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;293;2510.037,174.3127;Inherit;False;290;WaterLineRGBA;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;2759.774,-2612.093;Inherit;False;117;depthFromOrigin;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;235;2846.31,-851.7728;Inherit;False;False;False;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;288;1129.699,-917.2913;Inherit;False;Property;_WaterLine;WaterLine;36;0;Create;True;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;111;2783.801,-2534.677;Inherit;False;Property;_FogCutoutNear;FogCutoutNear;14;0;Create;True;0;0;False;0;False;0;9.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;131;2745.107,-2737.904;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;327;1850.955,-1421.474;Inherit;False;DarkEdge;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;255;2883.136,-972.9309;Inherit;False;256;Fresnel1Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;263;3491.806,-1332.8;Inherit;False;Constant;_Float0;Float 0;27;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;264;3513.927,-1195.864;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;112;2791.801,-2451.677;Inherit;False;Property;_FogCutoutFar;FogCutoutFar;16;0;Create;True;0;0;False;0;False;0.5;17.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;2400.497,7.712502;Inherit;False;206;AO;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;294;3015.332,302.3891;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;331;3027.705,425.0236;Inherit;False;327;DarkEdge;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;201;2281.386,-197.2896;Inherit;True;Property;_NRM;NRM;23;0;Create;True;0;0;False;0;False;-1;None;c04dab43891526e4d900962f1e62370a;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;301;4173.146,-1111.73;Inherit;False;Property;_FakeEmissiveCutoffMin;FakeEmissiveCutoffMin;46;0;Create;True;0;0;False;0;False;0.61;0.61;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;307;4179.146,-1034.73;Inherit;False;Property;_FakeEmissiveCutoffMax;FakeEmissiveCutoffMax;44;0;Create;True;0;0;False;0;False;0.61;0.61;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;236;3116.537,-760.0345;Inherit;False;Property;_EmissiveColor;EmissiveColor;26;0;Create;True;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;138;2978.774,-2606.093;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;265;3652.806,-1328.8;Inherit;False;Property;_Fresnel1;Fresnel1;26;0;Create;True;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;242;3102.589,-871.4192;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;296;1833.161,-905.3169;Inherit;False;TopplePOP_Vignette;-1;;46;b5e3db5f234f549459a4968c90a89f7e;0;1;1;COLOR;0,0,0,0;False;2;COLOR;0;FLOAT;37
Node;AmplifyShaderEditor.GetLocalVarNode;303;4394.146,-1168.73;Inherit;False;299;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;136;2936.774,-2734.093;Inherit;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;334;3284.705,334.0236;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;272;2804.549,-102.6216;Inherit;False;TopplePOP_ToonShade;5;;47;9b2a0fab25723f846a712419be9b67f4;0;4;23;FLOAT3;0,0,0;False;21;FLOAT;1;False;9;COLOR;0.5,0.5,0.5,0;False;28;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;295;3027.332,124.3891;Inherit;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;306;4611.146,-1171.73;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;266;3780.353,-1201.311;Inherit;False;Fresnel2Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;137;2931.774,-2875.093;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;297;2079.212,-686.1146;Inherit;False;VigFinal;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;237;3394.537,-806.0345;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;135;3139.774,-2750.093;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;302;4573.146,-1031.73;Inherit;False;Property;_FakeEmissiveAmt;FakeEmissiveAmt;45;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;269;3381.436,-1034.16;Inherit;False;Property;_Fresnel2Color;Fresnel2Color;25;0;Create;True;0;0;False;0;False;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;313;4764.831,-1167.114;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;140;3271.774,-2879.093;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;268;3643.436,-829.1602;Inherit;False;266;Fresnel2Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;240;3567.537,-738.0345;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;278;3272.772,-2445.102;Inherit;False;297;VigFinal;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;338;3154.8,-475.9186;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;291;3254.973,-45.1286;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;281;4144.423,-583.3757;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;310;4897.831,-1166.114;Inherit;False;FakeEmissiveAlpha;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;289;3417.425,-153.9353;Inherit;False;Property;_Keyword1;Keyword 1;36;0;Create;True;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Reference;288;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;238;3714.537,-738.0345;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;273;3819.459,-985.457;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;276;3454.915,-2787.731;Inherit;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;251;3947.589,-824.4189;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;141;3620.182,-2878.088;Inherit;True;fogColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;317;4372.613,-148.243;Inherit;False;310;FakeEmissiveAlpha;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;318;4357.313,-248.243;Inherit;False;299;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;280;4286.423,-411.3757;Inherit;False;Property;_Unlit;Unlit;35;0;Create;True;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;312;4184.131,-542.1135;Inherit;False;310;FakeEmissiveAlpha;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;314;4168.831,-642.1135;Inherit;False;299;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;270;4141.437,-752.1602;Inherit;False;Property;_Fresnel2;Fresnel2;31;0;Create;True;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;143;4101.839,-6.172538;Inherit;False;141;fogColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;316;4598.613,-301.243;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;315;4751.247,-426.4766;Inherit;False;Property;_UseFakeEmissive;UseFakeEmissive;43;0;Create;True;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Reference;300;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;336;4836.979,-666.1906;Inherit;False;327;DarkEdge;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;144;4276.839,57.82746;Inherit;False;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;311;4435.131,-703.1135;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;113;4215.834,137.9269;Inherit;False;Property;_FogAmount;FogAmount;21;0;Create;True;0;0;False;0;False;0.5;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;300;4660.75,-804.8301;Inherit;False;Property;_UseFakeEmissive;UseFakeEmissive;43;0;Create;True;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;337;5114.979,-450.1906;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;145;4499.538,76.12745;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;340;5256.943,-243.1701;Inherit;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;142;4822.628,-15.36746;Inherit;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;162;2358.429,-3278.974;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1000;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;335;5014.979,-637.1906;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;308;4890.146,-1018.73;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;223;409.5152,-602.8181;Inherit;False;albedo_alpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;230;24.97705,-537.129;Inherit;False;Constant;_Float1;Float 1;22;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;124;2528.498,-3211.165;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;161;2222.429,-3251.974;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;500;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;229;171.9771,-612.129;Inherit;False;Property;_Keyword0;Keyword 0;21;0;Create;True;0;0;False;0;False;0;0;0;True;UseUV2Albedo;Toggle;2;Key0;Key1;Reference;228;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;279;5439.547,-704.0439;Inherit;False;Property;_UseFog;UseFog;10;0;Create;True;0;0;False;0;False;1;1;1;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;339;5485.182,-431.5353;Inherit;False;Property;_UseFog1;UseFog;10;0;Create;True;0;0;False;0;False;1;1;1;True;;Toggle;2;Key0;Key1;Reference;279;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;160;2328.429,-3147.974;Inherit;False;Property;_yOffset;yOffset;22;0;Create;True;0;0;False;0;False;-500;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;292;2948.973,22.8714;Inherit;False;Constant;_Float2;Float 2;37;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;114;2684.311,-3199.758;Inherit;False;fogYOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;5818.317,-641.3394;Half;False;True;-1;4;ASEMaterialInspector;0;0;CustomLighting;Custom/StoryEnvironment;False;False;False;False;False;False;True;True;True;True;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-0.05;1,0,0,1;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;227;0;231;0
WireConnection;227;1;298;0
WireConnection;226;0;231;0
WireConnection;226;1;298;0
WireConnection;228;1;227;0
WireConnection;228;0;226;0
WireConnection;1;1;228;0
WireConnection;320;0;319;0
WireConnection;241;4;243;0
WireConnection;241;3;244;0
WireConnection;170;0;4;1
WireConnection;117;0;118;3
WireConnection;96;0;1;0
WireConnection;96;1;98;0
WireConnection;321;0;320;0
WireConnection;167;0;170;0
WireConnection;167;1;96;0
WireConnection;248;0;241;0
WireConnection;179;0;96;0
WireConnection;179;1;167;0
WireConnection;179;2;4;1
WireConnection;129;0;128;0
WireConnection;129;1;101;0
WireConnection;129;2;102;0
WireConnection;232;1;228;0
WireConnection;249;0;248;0
WireConnection;249;1;250;0
WireConnection;322;0;321;0
WireConnection;122;0;126;0
WireConnection;122;1;101;0
WireConnection;122;2;102;0
WireConnection;323;0;322;0
WireConnection;287;18;179;0
WireConnection;127;0;108;0
WireConnection;127;1;109;0
WireConnection;127;2;129;0
WireConnection;149;1;103;0
WireConnection;252;1;253;0
WireConnection;252;0;249;0
WireConnection;260;4;259;0
WireConnection;260;3;258;0
WireConnection;233;0;232;0
WireConnection;148;1;104;0
WireConnection;121;0;105;0
WireConnection;121;1;107;0
WireConnection;121;2;122;0
WireConnection;262;0;260;0
WireConnection;256;0;252;0
WireConnection;119;0;121;0
WireConnection;120;0;127;0
WireConnection;132;0;163;2
WireConnection;132;1;148;0
WireConnection;132;2;149;0
WireConnection;13;1;3;1
WireConnection;13;2;7;0
WireConnection;325;0;323;0
WireConnection;325;1;324;1
WireConnection;325;2;324;2
WireConnection;290;0;287;0
WireConnection;206;0;13;0
WireConnection;299;0;1;0
WireConnection;235;0;234;0
WireConnection;288;1;179;0
WireConnection;288;0;287;0
WireConnection;131;0;119;0
WireConnection;131;1;120;0
WireConnection;131;2;132;0
WireConnection;327;0;325;0
WireConnection;264;0;262;0
WireConnection;264;1;261;0
WireConnection;294;0;293;0
WireConnection;138;0;139;0
WireConnection;138;1;111;0
WireConnection;138;2;112;0
WireConnection;265;1;263;0
WireConnection;265;0;264;0
WireConnection;242;0;255;0
WireConnection;242;1;235;0
WireConnection;296;1;288;0
WireConnection;136;0;131;0
WireConnection;334;0;294;0
WireConnection;334;1;331;0
WireConnection;272;23;201;0
WireConnection;272;21;207;0
WireConnection;272;9;296;0
WireConnection;295;0;293;0
WireConnection;306;0;303;0
WireConnection;306;1;301;0
WireConnection;306;2;307;0
WireConnection;266;0;265;0
WireConnection;137;0;131;0
WireConnection;297;0;296;37
WireConnection;237;0;242;0
WireConnection;237;1;236;0
WireConnection;135;1;136;0
WireConnection;135;2;138;0
WireConnection;313;0;306;0
WireConnection;313;1;302;0
WireConnection;140;0;137;0
WireConnection;140;3;135;0
WireConnection;240;0;237;0
WireConnection;240;1;236;4
WireConnection;338;0;296;0
WireConnection;291;0;272;0
WireConnection;291;1;334;0
WireConnection;291;2;295;0
WireConnection;281;0;338;0
WireConnection;310;0;313;0
WireConnection;289;1;272;0
WireConnection;289;0;291;0
WireConnection;238;0;240;0
WireConnection;273;0;269;0
WireConnection;273;1;268;0
WireConnection;276;0;140;0
WireConnection;276;2;278;0
WireConnection;251;0;238;0
WireConnection;251;1;273;0
WireConnection;251;2;268;0
WireConnection;141;0;276;0
WireConnection;280;1;289;0
WireConnection;280;0;281;0
WireConnection;270;1;238;0
WireConnection;270;0;251;0
WireConnection;316;0;280;0
WireConnection;316;1;318;0
WireConnection;316;2;317;0
WireConnection;315;1;280;0
WireConnection;315;0;316;0
WireConnection;144;0;143;0
WireConnection;311;0;270;0
WireConnection;311;1;314;0
WireConnection;311;2;312;0
WireConnection;300;1;270;0
WireConnection;300;0;311;0
WireConnection;337;0;336;0
WireConnection;337;1;315;0
WireConnection;145;0;144;0
WireConnection;145;1;113;0
WireConnection;340;0;337;0
WireConnection;340;1;143;0
WireConnection;340;2;145;0
WireConnection;142;0;300;0
WireConnection;142;1;143;0
WireConnection;142;2;145;0
WireConnection;162;0;161;0
WireConnection;335;0;336;0
WireConnection;335;1;270;0
WireConnection;223;0;229;0
WireConnection;124;0;162;0
WireConnection;124;1;160;0
WireConnection;161;0;118;2
WireConnection;229;1;1;4
WireConnection;229;0;230;0
WireConnection;279;1;300;0
WireConnection;279;0;142;0
WireConnection;339;1;337;0
WireConnection;339;0;340;0
WireConnection;114;0;124;0
WireConnection;0;0;279;0
WireConnection;0;2;335;0
WireConnection;0;13;339;0
ASEEND*/
//CHKSM=BFF37090DA6126E6F56F083AE439784874C4A21D