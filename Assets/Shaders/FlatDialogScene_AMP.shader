// Made with Amplify Shader Editor v1.9.1.8
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Unlit/FlatDialogScene"
{
	Properties
	{
		_WorldXYOffset("WorldXYOffset", Vector) = (0.5,0.45,0,0)
		_TextureScale("TextureScale", Vector) = (0.265,0.46,0,0)
		[Toggle(_VIGNETTE_ON)] _Vignette("Vignette", Float) = 0
		[Toggle(_USEUVSINSTEADOFWORLDPOS_ON)] _UseUVsInsteadOfWorldPos("UseUVsInsteadOfWorldPos", Float) = 0
		_mainTex("mainTex", 2D) = "white" {}
		_ColorTint("ColorTint", Color) = (1,1,1,1)
		_TintOverridesColor("TintOverridesColor", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _VIGNETTE_ON
		#pragma shader_feature_local _USEUVSINSTEADOFWORLDPOS_ON
		#define ASE_USING_SAMPLING_MACROS 1
		#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
		#else//ASE Sampling Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
		#endif//ASE Sampling Macros

		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform float _VignetteIntensity;
		uniform float _VignetteLerpPower;
		uniform float4 _ColorTint;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_mainTex);
		uniform float2 _TextureScale;
		uniform float2 _WorldXYOffset;
		uniform float4 _mainTex_ST;
		SamplerState sampler_mainTex;
		uniform float _TintOverridesColor;
		uniform float4 _VignetteColor;
		uniform float _VignetteSize;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 temp_output_9_0 = ( ( (ase_worldPos).xy * ( _TextureScale * float2( 0.1,0.1 ) ) ) + _WorldXYOffset );
			float2 ifLocalVar22 = 0;
			if( ase_worldPos.y > -200.0 )
				ifLocalVar22 = temp_output_9_0;
			else if( ase_worldPos.y < -200.0 )
				ifLocalVar22 = ( temp_output_9_0 + float2( 0,23 ) );
			float2 uv_mainTex = i.uv_texcoord * _mainTex_ST.xy + _mainTex_ST.zw;
			#ifdef _USEUVSINSTEADOFWORLDPOS_ON
				float2 staticSwitch25 = uv_mainTex;
			#else
				float2 staticSwitch25 = ifLocalVar22;
			#endif
			float4 tex2DNode2 = SAMPLE_TEXTURE2D( _mainTex, sampler_mainTex, staticSwitch25 );
			float4 lerpResult37 = lerp( ( _ColorTint * tex2DNode2 ) , _ColorTint , _TintOverridesColor);
			float4 temp_output_1_0_g1 = lerpResult37;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 VignetteCoord7_g1 = ( ( (ase_screenPosNorm).xy / ase_screenPosNorm.w ) - float2( 0.5,0.5 ) );
			float dotResult9_g1 = dot( VignetteCoord7_g1 , VignetteCoord7_g1 );
			float Vignette_rf13_g1 = ( sqrt( dotResult9_g1 ) * _VignetteSize );
			float Vignette_rf_2_117_g1 = ( ( Vignette_rf13_g1 * Vignette_rf13_g1 ) + 1.0 );
			float Vignette_e22_g1 = ( 1.0 / pow( Vignette_rf_2_117_g1 , _VignetteIntensity ) );
			float temp_output_29_0_g1 = pow( ( _VignetteColor.a * ( 1.0 - Vignette_e22_g1 ) ) , _VignetteLerpPower );
			float4 lerpResult35_g1 = lerp( temp_output_1_0_g1 , float4( ( (temp_output_1_0_g1).rgb * (_VignetteColor).rgb ) , 0.0 ) , temp_output_29_0_g1);
			#ifdef _VIGNETTE_ON
				float4 staticSwitch21 = saturate( lerpResult35_g1 );
			#else
				float4 staticSwitch21 = lerpResult37;
			#endif
			o.Emission = staticSwitch21.rgb;
			o.Alpha = ( tex2DNode2.a * _ColorTint.a );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit alpha:fade keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 screenPos : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19108
Node;AmplifyShaderEditor.Vector2Node;13;-1214.3,-395.1;Inherit;False;Property;_TextureScale;TextureScale;1;0;Create;True;0;0;0;False;0;False;0.265,0.46;0.25,0.25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.WorldPosInputsNode;4;-1436.3,-553.1;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;5;-1040.3,-552.1;Inherit;False;True;True;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-1012.3,-410.1;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.1,0.1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;27;-1170.802,-104.4025;Inherit;True;Property;_mainTex;mainTex;4;0;Create;True;0;0;0;False;0;False;None;52eb43f71e4498440a8be0c66bbc276f;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.Vector2Node;10;-1216.3,-275.1;Inherit;False;Property;_WorldXYOffset;WorldXYOffset;0;0;Create;True;0;0;0;False;0;False;0.5,0.45;0.5,0.25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-831.3,-549.1;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;24;-665.0551,-921.8026;Inherit;False;617.8889;339.3693;Added so world pos offset doesnt break dialog scene presentation;4;23;22;18;19;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-690.3,-541.0999;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;19;-613.3,-744.1;Inherit;False;Constant;_Vector0;Vector 0;5;0;Create;True;0;0;0;False;0;False;0,23;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.WireNode;30;-872.9138,-248.0584;Inherit;False;1;0;SAMPLER2D;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.WireNode;31;-655.278,-260.0019;Inherit;False;1;0;SAMPLER2D;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.WorldPosInputsNode;23;-615.0551,-871.8026;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;18;-405.3,-692.1;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ConditionalIfNode;22;-238.0551,-805.8026;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;-200;False;2;FLOAT2;0,0;False;3;FLOAT;0;False;4;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;29;-603.4478,-251.964;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;25;-367.0656,-267.3427;Inherit;False;Property;_UseUVsInsteadOfWorldPos;UseUVsInsteadOfWorldPos;3;0;Create;True;0;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;All;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;2;-40.5,-98;Inherit;True;Property;_asd;asd;0;0;Create;True;0;0;0;False;0;False;-1;None;1c81b718b319ed548b4089d24998296a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;35;182.9734,-345.9618;Inherit;False;Property;_ColorTint;ColorTint;5;0;Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,0.7450981;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;834.4078,121.5997;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureTransformNode;26;-824.2645,-229.1449;Inherit;False;-1;False;1;0;SAMPLER2D;;False;2;FLOAT2;0;FLOAT2;1
Node;AmplifyShaderEditor.StaticSwitch;21;1051.356,-79.11317;Inherit;False;Property;_Vignette;Vignette;2;0;Create;True;0;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1301.875,-115.5221;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;Unlit/FlatDialogScene;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;;0;False;;False;0;False;;0;False;;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;12;all;True;True;True;True;0;False;;False;0;False;;255;False;;255;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;True;2;5;False;;10;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;486.0731,-323.1029;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;37;619.267,-216.6183;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;20;769.1559,-17.41315;Inherit;False;TopplePOP_Vignette;-1;;1;b5e3db5f234f549459a4968c90a89f7e;0;1;1;COLOR;0,0,0,0;False;2;COLOR;0;FLOAT;37
Node;AmplifyShaderEditor.RangedFloatNode;38;272.627,74.34691;Inherit;False;Property;_TintOverridesColor;TintOverridesColor;6;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
WireConnection;5;0;4;0
WireConnection;14;0;13;0
WireConnection;6;0;5;0
WireConnection;6;1;14;0
WireConnection;9;0;6;0
WireConnection;9;1;10;0
WireConnection;30;0;27;0
WireConnection;31;0;30;0
WireConnection;18;0;9;0
WireConnection;18;1;19;0
WireConnection;22;0;23;2
WireConnection;22;2;9;0
WireConnection;22;4;18;0
WireConnection;29;2;31;0
WireConnection;25;1;22;0
WireConnection;25;0;29;0
WireConnection;2;0;27;0
WireConnection;2;1;25;0
WireConnection;32;0;2;4
WireConnection;32;1;35;4
WireConnection;26;0;27;0
WireConnection;21;1;37;0
WireConnection;21;0;20;0
WireConnection;0;2;21;0
WireConnection;0;9;32;0
WireConnection;34;0;35;0
WireConnection;34;1;2;0
WireConnection;37;0;34;0
WireConnection;37;1;35;0
WireConnection;37;2;38;0
WireConnection;20;1;37;0
ASEEND*/
//CHKSM=D4BE75AA55D24BC8313D208C14A245F65D44AA9F