// Made with Amplify Shader Editor v1.9.4.4
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GrumpyFaceStory"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_EyeColor("EyeColor", Color) = (1,1,1,0)
		_OverallBrightness("OverallBrightness", Float) = 0.5
		_EyeOutlineBrightness("EyeOutlineBrightness", Range( 0 , 1)) = 0
		_EyelidBrightness("EyelidBrightness", Range( 0 , 1)) = 0
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_isBoost("isBoost", Range( 0 , 1)) = 0
		_FaceControlXOffset("FaceControlXOffset", Float) = 0
		_FaceControlYOffset("FaceControlYOffset", Float) = 0
		_ColorOffset("ColorOffset", Int) = 0
		_Step("Step", Float) = 0
		_remapamt("remapamt", Vector) = (0,0,0,0)
		[PerRendererData][IntRange]_isFrozen("isFrozen", Range( 0 , 1)) = 0
		[PerRendererData]_isActivated("isActivated", Range( 0 , 1)) = 0
		_Color("Color", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		Stencil
		{
			Ref 128
			ReadMask 128
			WriteMask 128
			Comp Always
			Pass Replace
		}
		Blend SrcAlpha OneMinusSrcAlpha
		
		AlphaToMask On
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#define ASE_USING_SAMPLING_MACROS 1
		#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
		#else//ASE Sampling Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
		#endif//ASE Sampling Macros

		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		UNITY_DECLARE_TEX2D_NOSAMPLER(_MainTex);
		uniform float _FaceControlXOffset;
		uniform float _FaceControlYOffset;
		uniform float _isActivated;
		uniform int _ColorOffset;
		uniform float _isFrozen;
		SamplerState sampler_MainTex;
		uniform float4 _EyeColor;
		uniform float4 _Color;
		uniform float _EyeOutlineBrightness;
		uniform float _isBoost;
		uniform float _OverallBrightness;
		uniform float _EyelidBrightness;
		uniform float _Step;
		uniform float2 _remapamt;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 appendResult83 = (float2(_FaceControlXOffset , _FaceControlYOffset));
			float2 appendResult182 = (float2(( _FaceControlXOffset + 0.5 ) , -0.875));
			float2 lerpResult179 = lerp( appendResult83 , appendResult182 , _isActivated);
			float2 appendResult193 = (float2(0.0 , 0.0));
			float2 appendResult92 = (float2(0.0 , ( 1.0 - ( (( _ColorOffset >= 0 && _ColorOffset <= 4 ) ? (float)_ColorOffset :  0.0 ) * 0.125 ) )));
			float2 ifLocalVar191 = 0;
			if( _isActivated >= 1.0 )
				ifLocalVar191 = appendResult193;
			else
				ifLocalVar191 = appendResult92;
			float2 appendResult119 = (float2(0.0 , ( 1.0 - ( 7.0 * 0.125 ) )));
			float2 lerpResult175 = lerp( ( lerpResult179 + ifLocalVar191 ) , appendResult119 , _isFrozen);
			float2 uv_TexCoord82 = i.uv_texcoord + lerpResult175;
			float2 appendResult167 = (float2((0.001953125 + (round( (-2.0 + (sin( frac( _Time.y ) ) - -2.0) * (2.0 - -2.0) / (2.0 - -2.0)) ) - 0.0) * (-0.001953125 - 0.001953125) / (1.0 - 0.0)) , 0.0));
			float2 uv_TexCoord160 = i.uv_texcoord + ( lerpResult175 + appendResult167 );
			float2 uv_TexCoord127 = i.uv_texcoord * float2( 8,8 ) + float2( 0,-0.43 );
			float mask_grumpymouth143 = ( 1.0 - floor( distance( frac( uv_TexCoord127.y ) , -1.32 ) ) );
			float2 lerpResult141 = lerp( uv_TexCoord82 , uv_TexCoord160 , mask_grumpymouth143);
			float2 lerpResult177 = lerp( uv_TexCoord82 , lerpResult141 , _isFrozen);
			float2 BaseCoords150 = lerpResult177;
			float4 tex2DNode28 = SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, BaseCoords150 );
			float4 break86 = tex2DNode28;
			float4 temp_cast_1 = (1.0).xxxx;
			float4 lerpResult60 = lerp( ( _Color * _EyeOutlineBrightness ) , temp_cast_1 , _isBoost);
			float3 appendResult17 = (float3(lerpResult60.rgb));
			float4 GrumpyMask148 = tex2DNode28;
			float lerpResult219 = lerp( 0.0 , _OverallBrightness , (GrumpyMask148).g);
			float3 temp_output_195_0 = (( lerpResult219 * _Color )).rgb;
			float3 appendResult19 = (float3(( _EyelidBrightness * _Color ).rgb));
			clip( 0.5 - 0.5);
			o.Albedo = (( ( break86.r + break86.g + break86.b ) >= 1.5 && ( break86.r + break86.g + break86.b ) <= 3.0 ) ? _EyeColor :  float4( ( ( break86.r * appendResult17 ) + ( break86.g * temp_output_195_0 ) + ( break86.b * appendResult19 ) ) , 0.0 ) ).rgb;
			float temp_output_117_0 = saturate( step( _Step , (0.0 + (SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, BaseCoords150 ).a - _remapamt.x) * (255.0 - 0.0) / (_remapamt.y - _remapamt.x)) ) );
			o.Alpha = temp_output_117_0;
			clip( temp_output_117_0 - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19404
Node;AmplifyShaderEditor.CommentaryNode;152;-4761.513,251.8945;Inherit;False;1268.434;255.2095;Color Offset (row Position) (is set to zero during block activation);7;92;91;88;94;95;191;193;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;169;-4739.999,638.3049;Inherit;False;1534.929;305.0306;shake offset amount;7;136;163;135;165;159;168;167;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IntNode;95;-4673.513,315.0289;Inherit;False;Property;_ColorOffset;ColorOffset;9;0;Create;True;0;0;0;True;0;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.SimpleTimeNode;136;-4689.999,691.3049;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareWithRange;94;-4439.779,313.8945;Inherit;False;5;0;INT;0;False;1;INT;0;False;2;INT;4;False;3;INT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;153;-4483.192,-302.6978;Inherit;False;841.0371;453.5456;Column Position (bottom row forced during activation);6;179;83;80;178;182;184;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;151;-3570.308,-1011.318;Inherit;False;1072;461;Grumpy Mouth Mask;6;143;132;131;130;127;155;;1,1,1,1;0;0
Node;AmplifyShaderEditor.FractNode;163;-4482.07,702.7183;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;88;-4196.779,317.8944;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.125;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;157;-3772.407,-834.5278;Inherit;False;Constant;_Vector0;Vector 0;13;0;Create;True;0;0;0;False;0;False;0,-0.43;0,-0.43;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;79;-4512,-560;Inherit;True;Property;_FaceControlXOffset;FaceControlXOffset;7;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;127;-3496.753,-899.2999;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;8,8;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinOpNode;135;-4360.999,688.3049;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;91;-4055.779,340.8944;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;184;-4286.223,-38.67297;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;80;-4512,-320;Inherit;True;Property;_FaceControlYOffset;FaceControlYOffset;8;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;83;-4128.247,-171.1535;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;165;-4190.07,688.7183;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;-2;False;2;FLOAT;2;False;3;FLOAT;-2;False;4;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;193;-3825.547,290.8889;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;182;-4091.491,-54.80347;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;-0.875;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;92;-3823.779,396.8944;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FractNode;130;-3268.753,-887.2999;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;121;-3438.142,509.9828;Inherit;False;2;2;0;FLOAT;7;False;1;FLOAT;0.125;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;178;-4235.957,69.56873;Inherit;False;Property;_isActivated;isActivated;13;1;[PerRendererData];Create;True;0;0;0;True;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;191;-3660.547,271.8889;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DistanceOpNode;131;-3081.753,-889.2999;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;-1.32;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;179;-3876.144,-225.3412;Inherit;True;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RoundOpNode;159;-3914.28,689.3354;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;122;-3297.142,532.9828;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;119;-3125.641,466.4828;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FloorOpNode;132;-2902.753,-888.2999;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;174;-3304.472,-121.2426;Inherit;False;Property;_isFrozen;isFrozen;12;2;[PerRendererData];[IntRange];Create;True;0;0;0;True;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;168;-3718.601,688.9274;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.001953125;False;4;FLOAT;-0.001953125;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;90;-3456.457,-1.90576;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;155;-2720.234,-926.9143;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;167;-3366.07,691.7183;Inherit;True;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;175;-2997.941,-2.942444;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;161;-2870.27,368.9114;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;143;-2562.65,-928.2745;Inherit;False;mask_grumpymouth;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;160;-2714.87,323.7114;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;144;-2722.847,241.6632;Inherit;False;143;mask_grumpymouth;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;82;-2753.202,-50.28838;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;200;-2275.086,-73.06312;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;141;-2441.744,161.6631;Inherit;True;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;177;-2096.921,-73.35616;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;150;-1890.578,-44.55833;Inherit;False;BaseCoords;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;170;-1131.786,-303.444;Inherit;False;150;BaseCoords;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;27;-1146.148,-494.8557;Inherit;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;0;False;0;False;078e24ccfe3418d44b44d6d2bfc5bff7;078e24ccfe3418d44b44d6d2bfc5bff7;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SamplerNode;28;-853.963,-377.6949;Inherit;True;Property;_TextureSample1;Texture Sample 1;4;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;148;-507.7429,-550.9379;Inherit;False;GrumpyMask;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;217;-2000.064,367.3798;Inherit;False;148;GrumpyMask;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;210;-1654.412,657.5298;Inherit;False;Property;_Color;Color;15;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.6415094,0.6415094,0.6415094,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;218;-2005.064,445.3798;Inherit;False;False;True;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-934.6426,463.1649;Inherit;False;Property;_EyeOutlineBrightness;EyeOutlineBrightness;3;0;Create;True;0;0;0;False;0;False;0;0.86;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;-1966.169,238.0414;Inherit;False;Property;_OverallBrightness;OverallBrightness;2;0;Create;True;0;0;0;False;0;False;0.5;-0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;73;-1220.71,441.3639;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-938.1779,557.9619;Inherit;False;Property;_EyelidBrightness;EyelidBrightness;4;0;Create;True;0;0;0;False;0;False;0;0.81;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;61;-763.2321,64.22525;Inherit;False;Constant;_Float0;Float 0;7;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;220;-857.2786,368.1688;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;219;-1733.064,264.3798;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-912.2321,157.2253;Inherit;False;Property;_isBoost;isBoost;6;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;-1353.488,249.3583;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-868.9429,635.6648;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;60;-595.2323,48.22525;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;38;-249.2452,-24.98401;Inherit;False;208;179;red = box color;1;7;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ComponentMaskNode;195;-1139.042,241.8737;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;86;-560.1526,-380.6469;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.CommentaryNode;39;-251.2452,169.016;Inherit;False;212;185;green = outline color;1;9;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;17;-417.2452,47.01601;Inherit;False;FLOAT3;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;40;-254.2452,374.016;Inherit;False;212;185;blue = eyelid color;1;10;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;19;-445.2452,435.016;Inherit;False;FLOAT3;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;171;-382.7859,-836.444;Inherit;False;150;BaseCoords;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-201.2452,228.016;Inherit;False;2;2;0;FLOAT;1;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;37;-120.6994,-523.4101;Inherit;False;453.9189;399;Detect if White (R+G+B >= 3) = Eyeballs;2;32;30;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-203.2452,25.01599;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;115;-68.76749,-664.8291;Inherit;False;Property;_remapamt;remapamt;11;0;Create;True;0;0;0;False;0;False;0,0;0,135.6;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SamplerNode;99;-200.2239,-860.1149;Inherit;True;Property;_TextureSample2;Texture Sample 2;4;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Instance;28;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-198.2452,421.016;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-172.6995,-402.366;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;32;-95.68729,-308.5107;Inherit;False;Property;_EyeColor;EyeColor;1;0;Create;True;0;0;0;False;0;False;1,1,1,0;0.6415094,0.6415094,0.6415094,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;8;34.75476,173.016;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;114;153.2325,-697.8291;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;167;False;3;FLOAT;0;False;4;FLOAT;255;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;102;183.2157,-784.0886;Inherit;False;Property;_Step;Step;10;0;Create;True;0;0;0;False;0;False;0;0.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareWithRange;110;175.9474,-500.8254;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;1.5;False;2;FLOAT;3;False;3;COLOR;0,0,0,0;False;4;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;107;396.4605,-722.0789;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;208;-543.7979,-240.8035;Inherit;False;2;0;FLOAT;0.61;False;1;FLOAT;0.77;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;198;-1565.354,162.2295;Inherit;False;outlineBrightness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;117;973.2325,-668.8291;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;196;-1137.926,324.1346;Inherit;False;outlineColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;204;-469.3234,-141.8091;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClipNode;47;544.9177,-242.4723;Inherit;True;3;0;COLOR;0,0,0,0;False;1;FLOAT;0.5;False;2;FLOAT;0.5;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;194;-1651.277,847.1658;Inherit;False;Property;_OutlineColor;OutlineColor;14;0;Create;True;0;0;0;False;0;False;1,0,0.9208169,1;1,0,0.9208169,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;54;1232.403,-159.4032;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;GrumpyFaceStory;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;;0;False;;False;0;False;;0;False;;False;0;Custom;0.5;True;False;0;True;Transparent;;Geometry;All;12;all;True;True;True;True;0;False;;True;128;False;;128;False;;128;False;;7;False;;3;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;False;2;5;False;;10;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;5;-1;-1;-1;0;True;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;True;17;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;16;FLOAT4;0,0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;94;0;95;0
WireConnection;94;3;95;0
WireConnection;163;0;136;0
WireConnection;88;0;94;0
WireConnection;127;1;157;0
WireConnection;135;0;163;0
WireConnection;91;0;88;0
WireConnection;184;0;79;0
WireConnection;83;0;79;0
WireConnection;83;1;80;0
WireConnection;165;0;135;0
WireConnection;182;0;184;0
WireConnection;92;1;91;0
WireConnection;130;0;127;2
WireConnection;191;0;178;0
WireConnection;191;2;193;0
WireConnection;191;3;193;0
WireConnection;191;4;92;0
WireConnection;131;0;130;0
WireConnection;179;0;83;0
WireConnection;179;1;182;0
WireConnection;179;2;178;0
WireConnection;159;0;165;0
WireConnection;122;0;121;0
WireConnection;119;1;122;0
WireConnection;132;0;131;0
WireConnection;168;0;159;0
WireConnection;90;0;179;0
WireConnection;90;1;191;0
WireConnection;155;0;132;0
WireConnection;167;0;168;0
WireConnection;175;0;90;0
WireConnection;175;1;119;0
WireConnection;175;2;174;0
WireConnection;161;0;175;0
WireConnection;161;1;167;0
WireConnection;143;0;155;0
WireConnection;160;1;161;0
WireConnection;82;1;175;0
WireConnection;200;0;174;0
WireConnection;141;0;82;0
WireConnection;141;1;160;0
WireConnection;141;2;144;0
WireConnection;177;0;82;0
WireConnection;177;1;141;0
WireConnection;177;2;200;0
WireConnection;150;0;177;0
WireConnection;28;0;27;0
WireConnection;28;1;170;0
WireConnection;148;0;28;0
WireConnection;218;0;217;0
WireConnection;73;0;210;0
WireConnection;220;0;73;0
WireConnection;220;1;33;0
WireConnection;219;1;197;0
WireConnection;219;2;218;0
WireConnection;199;0;219;0
WireConnection;199;1;210;0
WireConnection;34;0;35;0
WireConnection;34;1;210;0
WireConnection;60;0;220;0
WireConnection;60;1;61;0
WireConnection;60;2;62;0
WireConnection;195;0;199;0
WireConnection;86;0;28;0
WireConnection;17;0;60;0
WireConnection;19;0;34;0
WireConnection;9;0;86;1
WireConnection;9;1;195;0
WireConnection;7;0;86;0
WireConnection;7;1;17;0
WireConnection;99;1;171;0
WireConnection;10;0;86;2
WireConnection;10;1;19;0
WireConnection;30;0;86;0
WireConnection;30;1;86;1
WireConnection;30;2;86;2
WireConnection;8;0;7;0
WireConnection;8;1;9;0
WireConnection;8;2;10;0
WireConnection;114;0;99;4
WireConnection;114;1;115;1
WireConnection;114;2;115;2
WireConnection;110;0;30;0
WireConnection;110;3;32;0
WireConnection;110;4;8;0
WireConnection;107;0;102;0
WireConnection;107;1;114;0
WireConnection;208;1;86;1
WireConnection;198;0;197;0
WireConnection;117;0;107;0
WireConnection;196;0;195;0
WireConnection;204;0;208;0
WireConnection;47;0;110;0
WireConnection;54;0;47;0
WireConnection;54;9;117;0
WireConnection;54;10;117;0
ASEEND*/
//CHKSM=38D49E4F67E91A2BB6AAEA0D54FABB5726E349A5