// Made with Amplify Shader Editor v1.9.1.8
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "StoryGradiantBackplate"
{
	Properties
	{
		_ColorLow("ColorLow", Color) = (1,0,0,1)
		_ColorHigh("ColorHigh", Color) = (1,1,1,0)
		_GradiantTightness("GradiantTightness", Float) = 1
		_GradiantOffset("GradiantOffset", Float) = 0
		[Toggle(_USEVIGNETTE_ON)] _UseVignette("UseVignette", Float) = 0
		[Toggle(_USETEXTURE_ON)] _UseTexture("UseTexture", Float) = 0
		_MainTex("Base Texture", 2D) = "black" {}
		_EdgeDarkenRange("EdgeDarkenRange", Vector) = (0,0,0,0)
		_EdgeDarkenAmount("EdgeDarkenAmount", Range( 0 , 1)) = 0
		_Rannge_InGame("Rannge_InGame", Vector) = (0,0,0,0)
		_Range_editor("Range_editor", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		ZWrite On
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_local __ _USEVIGNETTE_ON
		#pragma multi_compile_local __ _USETEXTURE_ON
		#define ASE_USING_SAMPLING_MACROS 1
		#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
		#else//ASE Sampling Macros
		#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
		#endif//ASE Sampling Macros

		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform float4 _ColorLow;
		uniform float4 _ColorHigh;
		uniform float _GradiantOffset;
		uniform float2 _Range_editor;
		uniform float2 _Rannge_InGame;
		uniform float _GradiantTightness;
		UNITY_DECLARE_TEX2D_NOSAMPLER(_MainTex);
		uniform float4 _MainTex_ST;
		SamplerState sampler_MainTex;
		uniform float2 _EdgeDarkenRange;
		uniform float _EdgeDarkenAmount;
		uniform float4 _VignetteColor;
		uniform float _VignetteSize;
		uniform float _VignetteIntensity;
		uniform float _VignetteLerpPower;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float temp_output_112_0 = (0.0 + (ase_worldPos.y - _Range_editor.x) * (1.0 - 0.0) / (_Range_editor.y - _Range_editor.x));
			float temp_output_117_0 = ( temp_output_112_0 + -12.5 );
			float ifLocalVar111 = 0;
			if( ase_worldPos.y >= -400.0 )
				ifLocalVar111 = temp_output_112_0;
			else
				ifLocalVar111 = (0.0 + (ase_worldPos.y - _Rannge_InGame.x) * (1.0 - 0.0) / (_Rannge_InGame.y - _Rannge_InGame.x));
			float ifLocalVar116 = 0;
			if( ase_worldPos.y >= 300.0 )
				ifLocalVar116 = temp_output_117_0;
			else
				ifLocalVar116 = ifLocalVar111;
			float4 lerpResult21 = lerp( _ColorLow , _ColorHigh , saturate( ( ( ( _GradiantOffset * 0.1 ) + ifLocalVar116 ) * _GradiantTightness ) ));
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			#ifdef _USETEXTURE_ON
				float4 staticSwitch67 = ( lerpResult21 * SAMPLE_TEXTURE2D( _MainTex, sampler_MainTex, uv_MainTex ) );
			#else
				float4 staticSwitch67 = lerpResult21;
			#endif
			float4 Albedo41 = staticSwitch67;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float smoothstepResult83 = smoothstep( _EdgeDarkenRange.x , _EdgeDarkenRange.y , ( 1.0 - distance( abs( (ase_screenPosNorm).x ) , 0.5 ) ));
			float4 lerpResult86 = lerp( Albedo41 , ( smoothstepResult83 * Albedo41 ) , _EdgeDarkenAmount);
			float2 VignetteCoord49 = ( ( (ase_screenPosNorm).xy / ase_screenPosNorm.w ) - float2( 0.5,0.5 ) );
			float dotResult51 = dot( VignetteCoord49 , VignetteCoord49 );
			float Vignette_rf55 = ( sqrt( dotResult51 ) * _VignetteSize );
			float Vignette_rf_2_159 = ( ( Vignette_rf55 * Vignette_rf55 ) + 1.0 );
			float Vignette_e64 = ( 1.0 / pow( Vignette_rf_2_159 , _VignetteIntensity ) );
			float4 lerpResult39 = lerp( lerpResult86 , float4( ( (lerpResult86).rgb * (_VignetteColor).rgb ) , 0.0 ) , pow( ( _VignetteColor.a * ( 1.0 - Vignette_e64 ) ) , _VignetteLerpPower ));
			float4 Albedo_PostVig40 = lerpResult39;
			#ifdef _USEVIGNETTE_ON
				float4 staticSwitch43 = Albedo_PostVig40;
			#else
				float4 staticSwitch43 = Albedo41;
			#endif
			o.Emission = staticSwitch43.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=19108
Node;AmplifyShaderEditor.CommentaryNode;44;-154.7098,-1979.478;Inherit;False;1211.253;999.0025;Vignette;20;64;63;62;61;60;59;58;57;56;55;54;53;52;51;50;49;48;47;46;45;Vignette;0.3962264,0,0.2693404,1;0;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;45;-104.7098,-1922.645;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;46;75.96217,-1913.478;Inherit;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;47;282.9622,-1929.478;Inherit;True;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;48;511.231,-1923.595;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;647.3843,-1913.21;Inherit;False;VignetteCoord;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;50;-137.4363,-1713.927;Inherit;False;49;VignetteCoord;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DotProductOpNode;51;54.40918,-1721.389;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;53;368.7216,-1625.188;Inherit;False;Global;_VignetteSize;_VignetteSize;21;0;Create;True;0;0;0;False;0;False;0;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SqrtOpNode;52;222.0552,-1717.42;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;647.7922,-1682.388;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;55;855.9811,-1688.554;Inherit;False;Vignette_rf;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25;118.1693,315.4002;Inherit;False;Property;_GradiantTightness;GradiantTightness;2;0;Create;True;0;0;0;False;0;False;1;1.57;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;56;153.1516,-1520.178;Inherit;False;55;Vignette_rf;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;180.7706,209.8003;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;57;361.051,-1530.355;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;91.75703,-217.5158;Inherit;False;Property;_ColorLow;ColorLow;0;0;Create;True;0;0;0;False;0;False;1,0,0,1;0.113256,0.113256,0.198,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;109;302.6169,190.9907;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;2;66.83989,-16.8694;Inherit;False;Property;_ColorHigh;ColorHigh;1;0;Create;True;0;0;0;False;0;False;1,1,1,0;0.7176471,0.6980392,0.8078432,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;58;510.7964,-1528.9;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;77;-2489.98,-769.8034;Float;True;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;21;362.6662,-21.81371;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;59;641.6422,-1530.355;Inherit;False;Vignette_rf_2_1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;78;-2081.876,-798.6658;Inherit;False;True;False;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-95.13361,-1185.023;Inherit;False;Global;_VignetteIntensity;_VignetteIntensity;22;0;Create;True;0;0;0;True;0;False;0;4.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;61;-71.85297,-1341.794;Inherit;False;59;Vignette_rf_2_1;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;79;-1795.158,-766.8083;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;68;661.7567,66.76263;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DistanceOpNode;80;-1628.153,-780.9511;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;62;133.862,-1280.04;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;67;833.9459,-34.7541;Inherit;False;Property;_UseTexture;UseTexture;5;0;Create;True;0;0;0;False;0;False;1;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;41;1077.01,-25.13294;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector2Node;82;-1463.759,-564.3271;Inherit;False;Property;_EdgeDarkenRange;EdgeDarkenRange;7;0;Create;True;0;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.CommentaryNode;28;-894.7189,-883.6025;Inherit;False;1928.705;548.0984;;14;40;39;37;38;36;34;33;35;32;30;31;29;84;86;Vignette Blend;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;81;-1420.805,-785.9838;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;63;375.1393,-1261.524;Inherit;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;64;609.83,-1244.155;Inherit;False;Vignette_e;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;83;-1201.759,-786.3273;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;32;-876.9008,-846.5026;Inherit;False;41;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-1465.521,-310.3561;Inherit;False;Property;_EdgeDarkenAmount;EdgeDarkenAmount;8;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-689.6732,-775.7236;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;29;-113.7189,-467.3757;Inherit;False;64;Vignette_e;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;30;-94.87382,-669.8645;Inherit;False;Global;_VignetteColor;_VignetteColor;5;0;Create;True;0;0;0;True;0;False;0.6037736,0.3730865,0.3730865,0;0.369,0,0.1476,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;86;-511.813,-810.0656;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;31;55.49219,-464.5857;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;33;197.6462,-750.9155;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;36;191.6462,-677.9155;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;230.3602,-562.8765;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;226.5621,-465.5038;Inherit;False;Global;_VignetteLerpPower;_VignetteLerpPower;5;0;Create;True;0;0;0;True;0;False;12;27.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;37;439.5463,-524.6915;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;398.3842,-750.5845;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;39;555.5851,-807.8774;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;803.9862,-809.0325;Inherit;False;Albedo_PostVig;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;42;1264.965,189.6556;Inherit;False;40;Albedo_PostVig;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;43;1275.825,-29.68662;Inherit;True;Property;_UseVignette;UseVignette;4;0;Create;True;0;0;0;False;0;False;1;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;4;-1848.809,50.75921;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1535.7,-72;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;StoryGradiantBackplate;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;1;False;;0;False;;False;0;False;;0;False;;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;12;all;True;True;True;True;0;False;;False;0;False;;255;False;;255;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;0;False;;False;2;15;10;25;False;0.5;False;0;0;False;;0;False;;0;0;False;;0;False;;0;False;;0;False;;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;;-1;0;False;;0;0;0;False;0.1;False;;0;False;;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.SamplerNode;65;383.5534,420.4868;Inherit;True;Property;_MainTex;Base Texture;6;0;Create;False;0;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-399.6859,105.0517;Inherit;False;Property;_GradiantOffset;GradiantOffset;3;0;Create;True;0;0;0;False;0;False;0;-8.96;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-222.7071,101.2539;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;-97.07932,211.4092;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;113;-1154.1,698.2427;Inherit;False;Property;_Rannge_InGame;Rannge_InGame;9;0;Create;True;0;0;0;False;0;False;0,0;-10,30;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCRemapNode;107;-911.9497,550.3;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-510;False;2;FLOAT;-468;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;116;-459.1596,197.2206;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;300;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;111;-667.3823,342.9939;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;-400;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;114;-1155.1,573.2428;Inherit;False;Property;_Range_editor;Range_editor;10;0;Create;True;0;0;0;False;0;False;0,0;-10,30;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RegisterLocalVarNode;106;-1431.777,282.9703;Inherit;False;worldY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;87;-1262.704,305.8829;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TFHCRemapNode;112;-909.7335,375.4121;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-10;False;2;FLOAT;40;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;117;-669.3546,219.5178;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;400;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;118;-891.9177,151.037;Inherit;False;Constant;_ReplayOffset;ReplayOffset;11;0;Create;True;0;0;0;False;0;False;-12.5;-12.5;0;0;0;1;FLOAT;0
WireConnection;46;0;45;0
WireConnection;47;0;46;0
WireConnection;47;1;45;4
WireConnection;48;0;47;0
WireConnection;49;0;48;0
WireConnection;51;0;50;0
WireConnection;51;1;50;0
WireConnection;52;0;51;0
WireConnection;54;0;52;0
WireConnection;54;1;53;0
WireConnection;55;0;54;0
WireConnection;24;0;26;0
WireConnection;24;1;25;0
WireConnection;57;0;56;0
WireConnection;57;1;56;0
WireConnection;109;0;24;0
WireConnection;58;0;57;0
WireConnection;21;0;1;0
WireConnection;21;1;2;0
WireConnection;21;2;109;0
WireConnection;59;0;58;0
WireConnection;78;0;77;0
WireConnection;79;0;78;0
WireConnection;68;0;21;0
WireConnection;68;1;65;0
WireConnection;80;0;79;0
WireConnection;62;0;61;0
WireConnection;62;1;60;0
WireConnection;67;1;21;0
WireConnection;67;0;68;0
WireConnection;41;0;67;0
WireConnection;81;0;80;0
WireConnection;63;1;62;0
WireConnection;64;0;63;0
WireConnection;83;0;81;0
WireConnection;83;1;82;1
WireConnection;83;2;82;2
WireConnection;84;0;83;0
WireConnection;84;1;32;0
WireConnection;86;0;32;0
WireConnection;86;1;84;0
WireConnection;86;2;85;0
WireConnection;31;0;29;0
WireConnection;33;0;86;0
WireConnection;36;0;30;0
WireConnection;34;0;30;4
WireConnection;34;1;31;0
WireConnection;37;0;34;0
WireConnection;37;1;35;0
WireConnection;38;0;33;0
WireConnection;38;1;36;0
WireConnection;39;0;86;0
WireConnection;39;1;38;0
WireConnection;39;2;37;0
WireConnection;40;0;39;0
WireConnection;43;1;41;0
WireConnection;43;0;42;0
WireConnection;0;2;43;0
WireConnection;115;0;27;0
WireConnection;26;0;115;0
WireConnection;26;1;116;0
WireConnection;107;0;87;2
WireConnection;107;1;113;1
WireConnection;107;2;113;2
WireConnection;116;0;87;2
WireConnection;116;2;117;0
WireConnection;116;3;117;0
WireConnection;116;4;111;0
WireConnection;111;0;87;2
WireConnection;111;2;112;0
WireConnection;111;3;112;0
WireConnection;111;4;107;0
WireConnection;106;0;87;2
WireConnection;112;0;87;2
WireConnection;112;1;114;1
WireConnection;112;2;114;2
WireConnection;117;0;112;0
WireConnection;117;1;118;0
ASEEND*/
//CHKSM=611AF1A30457AE5B3C1B6D815BD888665541B50C